﻿using Alisson.Core.Factories;
using Alisson.Core.Services.Factories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alisson.Core.Models
{
    public abstract class Commodity: BaseObject
    {

        public int CommodityType { get; set; }
        public string Description { get; set; }
        public int m3 { get; set; }
        public string Name { get; set; }
        public bool Start { get; set; }
        public int StartPrice { get; set; }
        public double Weight { get; set; }

        public Commodity() : base() { }

        public Commodity(int id) : base(id) { }

        // If its a product, gets the raw materials used to produce it. If it's a raw material, get what products can be produced using it.
        public abstract IEnumerable<Commodity> getPartnerCommodities();

        public abstract string GetImageName();

        protected static new IFactory<T> getFactory<T>() where T : BaseObject
        {
            return (IFactory<T>)new CommoditiesFactory();
        }

    }
}
