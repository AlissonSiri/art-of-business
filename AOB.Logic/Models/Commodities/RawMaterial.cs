﻿using Alisson.Core.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alisson.Core.Models.Commodities
{
    public class RawMaterial: Commodity
    {

        public override IEnumerable<Commodity> getPartnerCommodities()
        {
            IEnumerable<int> prodIDs = BaseRepository<ProductRawMaterial>.getAll(pr => pr.RawMaterial == this.ID).Select(pr => pr.Product);
            return BaseRepository<Commodity>.getAll(p => prodIDs.Contains(p.ID));
        }

        public override string GetImageName()
        {
            return "";
        }

    }
}
