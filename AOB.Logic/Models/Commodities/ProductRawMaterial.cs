﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alisson.Core.Models.Commodities
{
    public class ProductRawMaterial: BaseObject
    {
        public int Product { get; set; }
        public int Quantity { get; set; }
        public int RawMaterial { get; set; }

    }
}
