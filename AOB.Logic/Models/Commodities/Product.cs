﻿using Alisson.Core.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alisson.Core.Models.Commodities
{
    public class Product: Commodity
    {

        public override IEnumerable<Commodity> getPartnerCommodities()
        {
            IEnumerable<int> rawIDs = BaseRepository<ProductRawMaterial>.getAll(pr => pr.Product == this.ID).Select(pr => pr.RawMaterial);
            return BaseRepository<Commodity>.getAll(r => rawIDs.Contains(r.ID));
        }

        public override string GetImageName()
        {
            return "";
        }

    }
}
