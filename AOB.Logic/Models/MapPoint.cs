﻿using Alisson.Core.Repository;
using Alisson.Core.Services;
using Alisson.Core.Services.Factories;
using Alisson.Core.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alisson.Core.Models
{

    public class MapPoint: BaseObject
    {
        public int ObjectId { get; set; }
        public int ObjectType { get; set; }
        public int Population { get; set; }
        public int x { get; set; }
        public int y { get; set; }

        private bool IsAvailable = true;

        public MapPoint() : base() { }

        public MapPoint(int x, int y)
        {
            this.x = x;
            this.y = y;
            this.ObjectId = 0;
            this.ObjectType = 7;
        }

        public MapPoint(IMapObject obj, int x, int y)
        {
            this.x = x;
            this.y = y;
            this.setObject(obj);
        }

        public double DistanceFrom(MapPoint point)
        {
            return Helpers.DistanceBetweenPoints(this.x, point.x, this.y, point.y);
        }

        public static IEnumerable<MapPoint> GetAvailablePlaces(IMapObject obj)
        {
            return MapPoint.GetAvailablePlaces(obj, BaseRepository<MapPoint>.getAll());
        }

        public static IEnumerable<MapPoint> GetAvailablePlaces(IMapObject obj, IEnumerable<MapPoint> source)
        {
            return source.Where(m => m.IsAvailableForObject(obj));
        }

        public string getDisplayText()
        {
            return this.getMapObject().GetDisplayText() + String.Format(" ({0} | {0})", this.x, this.y);
        }

        public IMapObject getMapObject()
        {
            return MapObjectsFactory.create((ObjectTypes)this.ObjectType, this.ObjectId);
        }

        public IEnumerable<MapPoint> GetNeighbors(int radius)
        {
            return BaseRepository<MapPoint>.getAll(m => m.IsNeighboorOf(this, radius));
        }

        public IEnumerable<IEnumerable<MapPoint>> GetNeighborsMatrix(int radius)
        {
            IEnumerable<MapPoint> map = this.GetNeighbors(radius);
            // Let's discover the extremities of our matrix.
            int minX = map.Min(m => m.x);
            int maxX = map.Max(m => m.x);
            int minY = map.Min(m => m.y);
            int maxY = map.Max(m => m.y);
            List<List<MapPoint>> result = new List<List<MapPoint>>();
            // We must start Y reverse, because the list'll be read reversely...
            for (int y = maxY + 1; y --> minY; ) // These are the rows. Each Y position is a vector (List of MapPlaces) representing a row.
            {
                List<MapPoint> row = new List<MapPoint>();
                for (int x = minX; x < maxX + 1; x++) // These are the cells. Each X represents a point inside the Y vector.
                    row.Add(map.Where(m => m.x == x && m.y == y).First());
                result.Add(row);
            }
            return result;

        }

        public override ObjectTypes getObjectType()
        {
            return ObjectTypes.MapPoint;
        }

        public string getToolTip()
        {
            IMapObject mo = this.getMapObject();
            return String.Concat(mo.GetDisplayText(), Environment.NewLine, String.Format("x: {0}", this.x), Environment.NewLine, String.Format("y: {0}", this.y));
        }

        public bool IsAvailableForObject(IMapObject obj)
        {
            return this.IsAvailableForType(obj.getObjectType(), obj.GetRadiusStartLimit());
        }

        public bool IsAvailableForType(ObjectTypes t, int minDistance)
        {
            if (!this.IsAvailable) // If we already know this is not available, it's because we checked it before, so we don't have to check it again. If for any reason this can be available again, we should use Observer pattern to notify this MapPoint to check if it can be available again.
                return false;
            if (this.ObjectId != 0) // If this MapPoint already owns an object, its not available.
                return this.IsAvailable = false;
            // Check if there is any object of the same type within the area.
            return this.IsAvailable = BaseRepository<MapPoint>.getAll(m => m.ObjectType == (int)t && m.IsNeighboorOf(this, minDistance)).Count() == 0;
        }

        public bool IsNeighboorOf(MapPoint neighboor, int radius)
        {
            return this.IsWithinSquare(neighboor.x, neighboor.y, radius);
        }

        public bool IsWithinPerimeter(int maxX, int maxY, int minX, int minY)
        {
            return minX <= this.x && maxX >= this.x && minY <= this.y && maxY >= this.y;
        }

        public bool IsWithinSquare(int middleX, int middleY, int radius)
        {
            return this.IsWithinPerimeter(middleX + radius, middleY + radius, middleX - radius, middleY - radius);
        }

        public void setObject(IMapObject obj)
        {
            this.ObjectId = obj.ID;
            this.ObjectType = (int)obj.getObjectType();
        }

        public static MapPoint Where(IMapObject obj)
        {
            return BaseRepository<MapPoint>.getAll(m => m.ObjectId == obj.ID && m.ObjectType == (int)obj.getObjectType()).First();
        }

    }
}
