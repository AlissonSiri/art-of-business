﻿using Alisson.Core;
using Alisson.Core.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Alisson.Core.Models
{
    public class DepartmentType: BaseObject
    {

        public string Description { get; set; }
        public int MaxEmployees { get; set; }
        public int MaxLevel { get; set; }
        public double MultiplicationFactor { get; set; }
        public string Name { get; set; }
        public bool Start { get; set; }
        public int StartingEmployees { get; set; }

        public DepartmentType() : base() { }

        public DepartmentType(int id) : base(id) { }

        public double getEmployeeCost()
        {
            return this.MultiplicationFactor * GlobalVariables.BASE_EMPLOYEE_HIRING_COST;
        }

        public int getMaxEmployees()
        {
            return this.getMaxEmployees(this.MaxLevel);
        }

        public int getMaxEmployees(int level)
        {
            double baseV = Math.Pow(this.MultiplicationFactor * GlobalVariables.LEVEL_UPGRADE_EMPLOYEES_FACTOR, (level) / 10d);
            double powV = 1.3 - (level - 1) / (30d);
            return Convert.ToInt32(Math.Round(Math.Pow(baseV, powV)));
        }
    }
}