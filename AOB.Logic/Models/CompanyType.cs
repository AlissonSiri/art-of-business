﻿using Alisson.Core;
using Alisson.Core.Models.Commodities;
using Alisson.Core.Repository;
using Alisson.Core.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Alisson.Core.Models
{
    public class CompanyType: BaseObject
    {

        public string Description { get; set; }
        public string Name { get; set; }
        public int SegmentType { get; set; }
        public bool Status { get; set; }

        public CompanyType() : base() { }

        public CompanyType(int id) : base(id) { }

        public IEnumerable<Commodity> getCommodities(CommodityType t)
        {
            switch (t)
            {
                case CommodityType.Product: return this.getProducts();
                case CommodityType.RawMaterial: return this.getRawMaterial();
                default: throw new ArgumentOutOfRangeException("Tipo de commodity inválido!");

            }
        }

        public string GetDisplayName()
        {
            return String.Concat(this.GetSegmentName(), " de ", this.Name);
        }

        public string GetSegmentName()
        {
            switch ((SegmentTypes)this.SegmentType)
            {
                case SegmentTypes.Industry: return "Indústria";
                case SegmentTypes.Commerce: return "Comércio";
                case SegmentTypes.DistributionCenter: return "";
                default: throw new ArgumentOutOfRangeException("Tipo de segmento não possui um nome definido!");
            }
        }

        public static IEnumerable<CompanyType> getIndustries()
        {
            return BaseRepository<CompanyType>.getAll(ct => ct.SegmentType == (int)SegmentTypes.Industry && ct.Status);
        }

        public IEnumerable<Commodity> getProducts()
        {
            IEnumerable<int> commIds = BaseRepository<CompanyTypeProducts>.getAll(cp => cp.CompanyType == this.ID).Select(p => p.Product);
            return BaseRepository<Commodity>.getAll(comm => comm.CommodityType == (int)CommodityType.Product && commIds.Contains(comm.ID));
        }

        public IEnumerable<Commodity> getRawMaterial()
        {
            IEnumerable<Commodity> products = this.getProducts().ToList();
            return products.SelectMany(p => p.getPartnerCommodities());
        }

        public IEnumerable<Commodity> getStarterCommodities(CommodityType t)
        {
            return this.getCommodities(t).Where(c => c.Start);
        }

    }
}