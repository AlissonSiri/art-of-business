﻿using Alisson.Core.Repository;
using Alisson.Core.Services;
using Alisson.Core.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alisson.Core.Models.GameEvents
{
    public class HireEmployeeGameEvent: GameEvent
    {

        public HireEmployeeGameEvent() // We need this for the factory!
        {

        }

        public HireEmployeeGameEvent(int recurrence, Department d)
        {
            this.StartDate = DateTime.UtcNow;
            this.setEventType(EventTypes.HireEmployee);
            this.Recurrence = recurrence;
            this.TotalRecurrence = recurrence;
            this.setObject(d);
            this.Company = d.Company;
        }

        protected override void calculateFinishDate()
        {
            Department d = this.getObject<Department>();
            this.FinishDate = this.StartDate + d.getHiringEmployeesTime();
        }

        //protected override bool canFinish()
        //{
        //    Department d = this.getObject<Department>();
        //    if (d.hasMaxEmployees()) // At this point we are sure this department is not at max level, otherwise it wouldn't even start!
        //    {
        //        this.StatusMessage = "Evolua este departamento, quantidade máxima de funcionários atingida!";
        //        return false;
        //    }
        //    return true;
        //}

        protected override bool canStart()
        {
            Department d = this.getObject<Department>();
            Company c = d.getCompany();

            IEnumerable<GameEvent> list = BaseRepository<GameEvent>.getAll(ev => ev.ID != this.ID && ev.Company == this.Company && ev.Type == this.Type && ev.HasActiveStatus());
            if (list.Count() > 0)
            {
                this.StatusMessage = "Já há contratação em andamento!";
                return false;
            }

            // First we check if current department's employees + how much we want to hire can be supported by this department max level's employees limit!
            if (this.TotalRecurrence + d.Employees > d.getDepartmentType().getMaxEmployees())
            {
                this.StatusMessage = "Este departamento não possui espaço para contratar essa quantidade de funcionários!";
                return false;
            }

            // Then we check if current department's level can support total employees.
            if (this.TotalRecurrence > d.getMaxEmployeesLeft())
            {
                this.StatusMessage = "Evolua este departamento, quantidade máxima de funcionários atingida!";
                return false;
            }

            double cost = Math.Round(d.getDepartmentType().getEmployeeCost() * this.Recurrence, 2);
            if (c.CurrentMoney < cost)
            {
                this.StatusMessage = "Sem dinheiro suficiente para contratar funcionários para esse departamento!";
                return false;
            }
            return true;
        }

        protected override void execute()
        {
            Department d = this.getObject<Department>();
            d.Employees++;
            d.save();
        }

        public override string GetDescription()
        {
            Department d = this.getObject<Department>();
            string result;
            int perc = (int)Math.Round(this.getRemainingTime().TotalMinutes / d.getHiringEmployeesTime().TotalMinutes * 100);
            if (perc < 25)
                result = String.Format("Realizando exames de admissão do candidato selecionado para {0}", d.ToString());
            else if (perc < 50)
                result = String.Format("Entrevistando candidatos para {0}", d.ToString());
            else if (perc < 75)
                result = String.Format("Agendando entrevista com candidatos para {0}", d.ToString());
            else
                result = String.Format("Procurando candidatos para {0}", d.ToString());

            return this.Recurrence > 1 ? String.Format("{0}. Mais {1} aguardando na fila!", result, this.Recurrence - 1) : result;
        }

        protected override EventTypes[] getSubjectEvents()
        {
            return new EventTypes[] { EventTypes.UpgradeDepartment };
        }

        protected override void start()
        {
            if (this.FirstRecurrence())
            {
                Department d = this.getObject<Department>();
                Company c = BaseRepository<Company>.getAll(comp => comp.ID == d.Company).First();
                double cost = Math.Round(d.getDepartmentType().getEmployeeCost() * this.Recurrence, 2);

                c.CurrentMoney -= cost;
                c.save();
            }
        }

    }
}
