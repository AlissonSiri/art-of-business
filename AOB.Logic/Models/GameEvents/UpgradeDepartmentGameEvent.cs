﻿using Alisson.Core.Repository;
using Alisson.Core.Services;
using Alisson.Core.Types;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Alisson.Core.Models.GameEvents
{
    public class UpgradeDepartmentGameEvent: GameEvent
    {

        public UpgradeDepartmentGameEvent() // We need this for the factory!
        {

        }

        public UpgradeDepartmentGameEvent(Department d)
        {
            this.Recurrence = 1;
            this.TotalRecurrence = 1;
            this.StartDate = DateTime.UtcNow;
            this.setEventType(EventTypes.UpgradeDepartment);
            this.setObject(d);
            this.Company = d.Company;
        }

        protected override void calculateFinishDate()
        {
            Department d = this.getObject<Department>();
            this.FinishDate = this.StartDate + d.getNextLevelTime();
        }

        protected override bool canStart()
        {
            Department d = this.getObject<Department>();
            if (d.isAtMaximumLevel())
            {
                this.StatusMessage =  "Este departamento já está no nível máximo!";
                return false;
            }

            IEnumerable<GameEvent> list = BaseRepository<GameEvent>.getAll(ev => ev.Company == this.Company && ev.Type == this.Type && ev.HasActiveStatus());
            if (list.Count() > 0)
            {
                this.StatusMessage = "Já existe um departamento em melhoria!";
                return false;
            }

            Company c = d.getCompany();
            double cost = d.getNextLevelCost();
            if (c.CurrentMoney < cost)
            {
                this.StatusMessage = "Sem dinheiro suficiente para evoluir este departamento!";
                return false;
            }
            return true;
        }

        protected override void execute()
        {
            Department d = this.getObject<Department>();
            d.CurrentLevel++;
            d.save();
        }

        public override string GetDescription()
        {
            Department d = this.getObject<Department>();
            return String.Format("Evoluindo {0} para nível {1}", d.ToString(), d.CurrentLevel + 1);
        }

        protected override void start()
        {
            Department d = this.getObject<Department>();
            Company c = d.getCompany();
            double cost = d.getNextLevelCost();
            c.CurrentMoney -= cost;
            c.save();
        }

    }
}
