﻿using Alisson.Core.Factories;
using Alisson.Core.Models.Commodities;
using Alisson.Core.Repository;
using Alisson.Core.Services.Factories;
using Alisson.Core.Types;
using Marimex.Core.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alisson.Core.Models
{
    [Table("npcs")]
    public abstract class Npc: BaseObject, IMapObject
    {

        public int Type { get; set; }

        public Npc() : base() { }

        public Npc(int id) : base(id) { }

        public static CommodityType DiscoverCommodityType(NpcTypes type)
        {
            switch (type)
            {
                case NpcTypes.Customer: return CommodityType.Product;
                case NpcTypes.Supplier: return CommodityType.RawMaterial;
                default: throw new ArgumentOutOfRangeException("Tipo de NPC não implementado para descoberta de tipo de commodity!");
            }
        }

        public IEnumerable<Commodity> getCommodities()
        {
            IEnumerable<int> commIds = BaseRepository<NpcCommodity>.getAll(cp => cp.Npc == this.ID).Select(p => p.Commodity);
            return BaseRepository<Commodity>.getAll(comm => comm.CommodityType == this.Type && commIds.Contains(comm.ID));
        }

        public CommodityType GetCommodityType()
        {
            return (CommodityType)this.Type;
        }

        public abstract string GetDescription();

        public abstract string GetDisplayText();

        protected static new IFactory<T> getFactory<T>() where T : BaseObject
        {
            return (IFactory<T>)new NpcsFactory();
        }

        public abstract string GetImageName();

        public int GetRadiusStartLimit()
        {
            return 2;
        }

        public abstract int GetQuantityPerPlayer(); // When a new user register, this is how many Customers we should create nearby.

        public abstract int GetPlayerRadiusSearch(); // To determine how many Customers the current player has nearby, this is the search radius.

        public abstract int GetTradingCommoditiesQuantity(); // How many commodities does this npc trade.

        public abstract string getTaskDescription();
    }
}
