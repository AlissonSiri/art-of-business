﻿using Alisson.Core;
using Alisson.Core.Repository;
using Alisson.Core.Types;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Alisson.Core.Models
{
    public class User: BaseObject
    {

        public string CompanyName { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }

        public User() : base() { }

        public User(int id) : base(id) { }

        public static User Login(string name, string password)
        {
            return BaseRepository<User>.getAll(u => u.Name == name && u.Password == password).First();
        }

        public IEnumerable<Company> getCompanies()
        {
            return BaseRepository<Company>.getAll(c => c.User == this.ID);
        }

        public override ObjectTypes getObjectType()
        {
            return ObjectTypes.User;
        }

    }
}