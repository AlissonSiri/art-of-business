﻿using Alisson.Core.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alisson.Core.Models.Npcs
{
    public class Supplier: Npc
    {

        public static readonly int TradingCommoditiesQuantity = 3;
        public static readonly int QuantityPerPlayer = 3; // When a new user register, this is how many Customers we should create nearby.
        public static readonly int PlayerRadiusSearch = 3; // To determine how many Customers the current player has nearby, this is the search radius.

        public Supplier() : base() { this.Type = (int)NpcTypes.Supplier; }

        public Supplier(int id) : base(id) { }

        public override string GetDescription()
        {
            return "Aqui encontra-se um fornecedor, que possui matéria-prima à venda. Boas compras!";
        }

        public override string GetDisplayText()
        {
            return "Fornecedor";
        }

        public override string GetImageName()
        {
            if (this.ID % 2 == 1)
                return "supplier1.png";
            else
                return "supplier2.png";
        }

        public override ObjectTypes getObjectType()
        {
            return ObjectTypes.Supplier;
        }

        public override int GetQuantityPerPlayer()
        {
            return QuantityPerPlayer;
        }

        public override int GetPlayerRadiusSearch()
        {
            return PlayerRadiusSearch;
        }

        public override int GetTradingCommoditiesQuantity()
        {
            return TradingCommoditiesQuantity;
        }

        public override string getTaskDescription()
        {
            return "Vendendo";
        }

    }
}
