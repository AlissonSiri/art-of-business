﻿using Alisson.Core.Models.Commodities;
using Alisson.Core.Repository;
using Alisson.Core.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Alisson.Core.Models;

namespace Alisson.Core.Models.Npcs
{
    public class Customer : Npc
    {

        public static readonly int TradingCommoditiesQuantity = 3;
        public static readonly int QuantityPerPlayer = 3; // When a new user register, this is how many Customers we should create nearby.
        public static readonly int PlayerRadiusSearch = 3; // To determine how many Customers the current player has nearby, this is the search radius.

        public Customer() : base() { this.Type = (int)NpcTypes.Customer; }

        public Customer(int id) : base(id) { }

        public override string GetDescription()
        {
            return "Aqui encontra-se um cliente, que está a procura de novos produtos. Faça sua oferta!";
        }

        public override string GetDisplayText()
        {
            return "Cliente";
        }

        public override string GetImageName()
        {
            if (this.ID % 2 == 1)
                return "customer1.png";
            else
                return "customer2.png";
        }

        public override ObjectTypes getObjectType()
        {
            return ObjectTypes.Customer;
        }

        public override string getTaskDescription()
        {
            return "Comprando";
        }

        public override int GetTradingCommoditiesQuantity()
        {
            return TradingCommoditiesQuantity;
        }

        public override int GetQuantityPerPlayer()
        {
            return QuantityPerPlayer;
        }

        public override int GetPlayerRadiusSearch()
        {
            return PlayerRadiusSearch;
        }

    }
}
