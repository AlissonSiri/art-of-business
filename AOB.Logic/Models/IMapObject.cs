﻿
namespace Alisson.Core.Models
{
    public interface IMapObject : IEntity
    {

        string GetDisplayText(); // Get a text for displaying to user.

        int GetRadiusStartLimit(); // When a new IMapObject is created, there can't exist another IMapObject of the same type within this radius.

        string GetImageName(); // Get the proper object image name, which must correspond to the img folder files.

        string GetDescription(); // Get the object description

    }

}
