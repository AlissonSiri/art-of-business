﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AOB.Core.Models
{
    public class DepartmentLevelConfigs: BaseObject
    {

        public decimal Cost { get; set; }
        public int DepartmentType { get; set; }
        public int Level { get; set; }
        public double MinutesDuration { get; set; }

        public TimeSpan DurationInTimeSpan()
        {
            return TimeSpan.FromMinutes(this.MinutesDuration);
        }

        public string GetFormatedCost()
        {
            return String.Format("B$ {######.##}", this.Cost);
        }

    }
}
