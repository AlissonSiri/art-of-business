﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alisson.Core.Models
{
    public class CompanyTypeProducts: BaseObject
    {

        public int CompanyType { get; set; }
        public int Product { get; set; }

        public CompanyTypeProducts() : base() { }

        public CompanyTypeProducts(int id) : base(id) { }

    }
}
