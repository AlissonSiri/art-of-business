﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alisson.Core.Models
{
    public class NpcCommodity: BaseObject
    {
        public int Npc { get; set; }
        public int Commodity { get; set; }

        public NpcCommodity() : base() { }

        public NpcCommodity(int id) : base(id) { }

    }
}
