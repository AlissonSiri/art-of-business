﻿using Alisson.Core;
using Alisson.Core.Repository;
using Alisson.Core.Services;
using Alisson.Core.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Alisson.Core.Models
{
    public class Department: BaseObject
    {

        public int Company { get; set; }
        public int CurrentLevel { get; set; }
        public int Employees { get; set; }
        public int Type { get; set; }

        public Department() : base() { }

        public Department(int id) : base(id) { }

        public DepartmentType getDepartmentType()
        {
            return BaseRepository<DepartmentType>.getAll().Where(dt => dt.ID == this.Type).First();
        }

        public Company getCompany()
        {
            return BaseRepository<Company>.getAll(comp => comp.ID == this.Company).First();
        }

        public TimeSpan getHiringEmployeesTime()
        {
            Department hr = BaseRepository<Department>.getAll(d => d.Company == this.Company && d.getDepartmentType().ID == (int) DepartmentTypes.HR).First();
            double minutes = GlobalVariables.BASE_EMPLOYEE_HIRING_MINUTES_DURATION / hr.Employees * this.getDepartmentType().MultiplicationFactor;
            return TimeSpan.FromSeconds(Math.Round(minutes * 60 / GlobalVariables.SERVER_SPEED));
        }

        public int getMaxEmployees()
        {
            return this.getDepartmentType().getMaxEmployees(this.CurrentLevel);
        }

        public int getMaxEmployeesLeft()
        {
            return this.getDepartmentType().getMaxEmployees(this.CurrentLevel) - this.Employees;
        }

        public double getNextLevelCost()
        {
            if (this.isAtMaximumLevel())
                throw new MethodAccessException("Este departamento já está no nível máximo!");
            DepartmentType type = this.getDepartmentType();
            double baseV = type.MultiplicationFactor * GlobalVariables.BASE_IMPORTANCE_MULTIPLIER;
            double powV = 1 + ((this.CurrentLevel) / GlobalVariables.LEVEL_UPGRADE_FACTOR);
            return Math.Round(Math.Pow(baseV, powV), 2);
        }

        public TimeSpan getNextLevelTime()
        {
            if (this.isAtMaximumLevel())
                throw new MethodAccessException("Este departamento já está no nível máximo!");
            DepartmentType type = this.getDepartmentType();
            double baseV = type.MultiplicationFactor * GlobalVariables.BASE_IMPORTANCE_MULTIPLIER;
            Department civil = BaseRepository<Department>.getAll().Where(c => c.Company == this.Company && c.Type == 2).First();
            double civilConstrFactor = 1 + (10 - civil.CurrentLevel) * (1d/6d);
            double powV = 1 + ((this.CurrentLevel) / GlobalVariables.LEVEL_UPGRADE_FACTOR);
            double secs = Math.Pow(baseV, powV) / (24000 / 86.4) * civilConstrFactor;
            return TimeSpan.FromSeconds(Math.Round(secs / GlobalVariables.SERVER_SPEED));
        }

        public override ObjectTypes getObjectType()
        {
            return ObjectTypes.Department;
        }

        public string getTypeName()
        {
            return BaseRepository<DepartmentType>.getAll().Where(dt => dt.ID == this.Type).First().Name;
        }

        public bool hasMaxEmployees()
        {
            return this.getMaxEmployees() == this.Employees;
        }

        public bool isAtMaximumLevel()
        {
            return this.CurrentLevel == this.getDepartmentType().MaxLevel;
        }

        public override string ToString()
        {
            return this.getDepartmentType().Name;
        }

    }
}