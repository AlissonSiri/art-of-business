﻿using Alisson.Core.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alisson.Core.Models
{
    public class EmptyMapObject: BaseObject, IMapObject
    {

        public EmptyMapObject(int objID)
        {
            this.ID = objID;
        }

        public string GetDescription()
        {
            return "Este é um local vazio, disponível para uma nova empresa!";
        }

        public string GetDisplayText()
        {
            return "Local vazio";
        }

        public string GetImageName()
        {
            return "grass1.png";
        }

        public override ObjectTypes getObjectType()
        {
            return ObjectTypes.EmptyMapObject;
        }

        public int GetRadiusStartLimit()
        {
            throw new NotImplementedException();
        }

    }
}
