﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alisson.Core.Models
{
    public class UserIp: BaseObject
    {

        public int UserId { get; set; }
        public string Ip { get; set; }

    }
}
