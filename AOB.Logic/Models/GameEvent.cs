﻿using Alisson.Core.IoC;
using Alisson.Core.Repository;
using Alisson.Core.Services.Commands.ExecuteEvents;
using Alisson.Core.Services;
using Alisson.Core.Services.Factories;
using Alisson.Core.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Marimex.Core.Attributes;
using Alisson.Core.Factories;

namespace Alisson.Core.Models
{
    [Table("game_events")]
    public abstract class GameEvent: BaseObject
    {
        public int Company { get; set; }
        public DateTime FinishDate { get; set; }
        public int ObjectId { get; set; }
        public int ObjectType { get; set; }
        public int Recurrence { get; set; }
        public DateTime StartDate { get; set; }
        public int Status { get; set; }
        public int TotalRecurrence { get; set; }
        public int Type { get; set; }

        protected static EventStatus[] valid = new EventStatus[] { EventStatus.Executing, EventStatus.Stopped };
        public string StatusMessage;

        private void AttachSubscribers()
        {
            // We'll get all executing/stopped events whose type interest to us. We won't care about its finish date, because the finish date can be changed by its subjects. If it finishes, nice. If not, sad ;/
            IEnumerable<GameEvent> events = BaseRepository<GameEvent>.getAll().Where(e => e.HasActiveStatus() && e.getSubjectEvents().Contains(this.getEventType()));
            foreach (GameEvent ev in events)
                this.Subscribe(ev);
        }

        protected override void beforeSaving()
        {
            if (this.ID == -1)
            {
                if (!this.canStart())
                    throw new GameEventException(this.StatusMessage);
                this.StatusMessage = "";
                this.start(); // If start fires an exception, we won't create this new event, and show error to player.
                this.calculateFinishDate();
            }

        }

        protected abstract void calculateFinishDate();

        protected virtual bool canFinish()
        {
            return true; // If the subclass doesn't implement validations, we assume it's ok.
        }

        protected virtual bool canStart()
        {
            return true; // If the subclass doesn't implement validations, we assume it's ok.
        }

        protected abstract void execute();

        public void finish()
        {
            if (this.canFinish())
            {
                this.StatusMessage = "";
                this.AttachSubscribers(); // We'll let other events know about our existence, since we're finishing, but only the ones which cares about our state.
                this.execute();
                this.Notify(); // We notify possible stopped actions which might depend on this current action's finish

                if (this.Recurrence > 1)
                    this.startNewRecurrence();
                else
                    this.setStatus(EventStatus.Finished); // If it's the last recurrence, we finish!
            }
            else
                this.stop(); // If this event doesn't pass Finish Validations, or a new Recurrence can't start, we stop (subscribing to another events) and wait.
        }

        public bool FirstRecurrence()
        {
            return this.Recurrence == this.TotalRecurrence;
        }

        public abstract string GetDescription();

        public EventTypes getEventType()
        {
            return (EventTypes) this.Type;
        }

        protected static new IFactory<T> getFactory<T>() where T : BaseObject
        {
            return (IFactory<T>)new GameEventsFactory();
        }

        public T getObject<T>() where T: BaseObject
        {
            return BaseRepository<T>.getAll().Where(o => o.ID == this.ObjectId && (int)o.getObjectType() == this.ObjectType).First();
        }

        public TimeSpan getRemainingTime()
        {
            if (DateTime.UtcNow > this.FinishDate) // First we check if FinishDate has passed
            {
                // We try to stop only if it's executing. If it can't finish, it'll be stopped and once stopped, we won't want to try to finish every time, we'll just wait for an update by other event.
                //if (this.getStatus() == EventStatus.evsExecuting)
                //{
                //    ExecuteEventsCommandData data = new ExecuteEventsCommandData(this);
                //    ExecuteEventsCommandHandler h = GenericFactory<ExecuteEventsCommandHandler>.GetDefault();
                //    h.Handle(data);
                //}
                return new TimeSpan(0); // Actually this line should never be reached.
            }
            return TimeSpan.FromSeconds(Math.Round((this.FinishDate - DateTime.UtcNow).TotalSeconds));
        }

        public EventStatus getStatus()
        {
            return (EventStatus)this.Status;
        }

        // This method return the type of events which this current event is interested in.
        // Example: Suppose we are processing a HireEmployee event, when we discovered that its target department
        // is full. Then we would like to look if its target department has an UpgradeDepartment event in progress.
        // If yes, we'll subscribe to this event, so when it finishes, we'll get notified to try finishing again
        // the process (event) of hiring an employee.
        protected virtual EventTypes[] getSubjectEvents()
        {
            return new EventTypes[] { };
        }

        public bool HasActiveStatus()
        {
             // A stopped event can resume ;)
            return valid.Contains(this.getStatus());
        }

        public bool HasPassedFinishDate()
        {
            return DateTime.UtcNow > this.FinishDate;
        }

        private void setStatus(EventStatus status)
        {
            this.Status = (int)status;
        }

        public void setEventType(EventTypes type)
        {
            this.Type = (int)type;
        }

        public void setObject(BaseObject obj)
        {
            this.ObjectId = obj.ID;
            this.ObjectType = (int)obj.getObjectType();
        }

        protected virtual void start()
        {
           // Should be implemented by subclass. Can't be abstract because we need GameEvent not to be abstract, since we instantite it
        }

        private void startNewRecurrence()
        {
            this.Recurrence--;
            this.StartDate = this.FinishDate; // The new recurrence will start at the previous Finish Date
            this.start();
            this.calculateFinishDate(); // Recalculate new Finish Date
        }

        public void stop()
        {
            this.Status = (int)EventStatus.Stopped;
            this.SubscribeToAll(); // Now we're stopped we should look for some other event that could help us to finish, or wait until some event look for us.
        }

        private void SubscribeToAll()
        {
            EventTypes[] types = this.getSubjectEvents();
            // We'll get all executing/stopped events whose type interest to us. We won't care about its finish date, because the finish date can be changed by its subjects. If it finishes, nice. If not, sad ;/
            IEnumerable<GameEvent> events = BaseRepository<GameEvent>.getAll().Where(e => e.HasActiveStatus() && types.Contains(e.getEventType()));
            foreach (GameEvent ev in events)
                ev.Subscribe(this);
        }


        public override void Update(ISubject sub)
        {
            throw new NotImplementedException();
        }
    }
}
