﻿using Alisson.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Alisson.Core.Repository;
using Alisson.Core.Types;
using Alisson.Core.Models.Commodities;
using Alisson.Core.Services.Factories;

namespace Alisson.Core.Models
{
    public class Company : BaseObject, IMapObject
    {
        public string BranchNick { get; set; }
        public double CurrentMoney { get; set; }
        public int Type { get; set; }
        public int User { get; set; }

        public Company() : base() { }

        public Company(int id) : base(id) { }

        public void createBaseDepartments()
        {
            IEnumerable<DepartmentType> deptypes = BaseRepository<DepartmentType>.getAll().Where(d => d.Start);
            foreach (DepartmentType deptype in deptypes)
            {
                Department dep = new Department();
                dep.Company = this.ID;
                dep.CurrentLevel = 1;
                dep.Employees = deptype.StartingEmployees;
                dep.Type = deptype.ID;
                BaseRepository<Department>.add(dep);
            }
        }

        public IEnumerable<Commodity> getCommodities(CommodityType t)
        {
            return this.getCompanyType().getCommodities(t);
        }

        public CompanyType getCompanyType()
        {
            return BaseRepository<CompanyType>.First(ct => ct.ID == this.Type);
        }

        public string GetDescription()
        {
            return "Um jogador qualquer";
        }

        public string GetDisplayText()
        {
            return String.Concat(this.getCompanyType().GetDisplayName(), " (", this.GetUser().Name, ")");
        }

        public string GetImageName()
        {
            if (this.getCompanyType().SegmentType == (int)SegmentTypes.Industry)
            {
                if (this.getTotalEmployees() < 300)
                    return "industry1.png";
                else
                    return "industry2.png";
            }
            throw new NotImplementedException("Tipo de segmento não possui ícones definidos!");
        }

        public override ObjectTypes getObjectType()
        {
            return ObjectTypes.Company;
        }

        public Npc GetNearestNpc(NpcTypes type, List<MapPoint> neighborhood = null)
        {
            Npc npc = NpcsFactory.create(type); // Create blank NPC
            MapPoint p = MapPoint.Where(this);
            List<MapPoint> npcs = BaseRepository<MapPoint>.getAll(mp => mp.ObjectType == (int)npc.getObjectType()).ToList();
            return (Npc) npcs.OrderBy(mp => mp.DistanceFrom(p)).First().getMapObject();
        }

        public int GetRadiusStartLimit()
        {
            return 3;
        }

        public IEnumerable<Commodity> getStarterCommodities(CommodityType t)
        {
            return this.getCompanyType().getStarterCommodities(t);
        }

        public int getTotalEmployees()
        {
            return BaseRepository<Department>.getAll().Where(d => d.Company == this.ID).Sum(s => s.Employees);
        }

        public User GetUser()
        {
            return BaseRepository<User>.getByID(this.User);
        }

    }
}