﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Alisson.Core.Services
{
    public class GlobalVariables
    {

        public const double BASE_IMPORTANCE_MULTIPLIER = 10000; // Department upgrade cost starts with something about this value
        public const double BASE_EMPLOYEE_HIRING_COST = 680;
        public const double BASE_EMPLOYEE_HIRING_MINUTES_DURATION = 14;
        public const double LEVEL_UPGRADE_FACTOR = 18; // The less is this value, the more is the difference of final cost/time between levels.
        public const double LEVEL_UPGRADE_EMPLOYEES_FACTOR = 100;
        public const int RADIUS_CLIENT = 3; // How large is the radius of a 'square' where we can't have two clients within the perimeter.
        public const int RADIUS_STARTING_USER = 2; // How large is the radius of a 'square' where we can't put a new user within the perimeter if there is another user.
        public const double SERVER_SPEED = 20; // If we change this value from 1 to 2, things will finish 2 times faster. For test purposes.
        public const double START_MONEY = 300000; // The const name explains for itself.

    }
}