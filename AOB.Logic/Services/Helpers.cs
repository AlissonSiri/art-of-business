﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Alisson.Core.Services
{
    public class AOBHelpers
    {

        public static string FormatToBrions(double value)
        {
            NumberFormatInfo format = (NumberFormatInfo)NumberFormatInfo.CurrentInfo.Clone();
            format.CurrencySymbol = "B$ ";
            format.CurrencyDecimalSeparator = ",";
            format.CurrencyGroupSeparator = ".";
            return value.ToString("C", format);
        }

    }

}
