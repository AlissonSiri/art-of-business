﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alisson.Core.Types
{

    public enum CommodityType { Product, RawMaterial }

    public enum DepartmentTypes
    {
        CustomerService = 1,
        CivilConstruction,
        Accounting,
        Stock,
        Labs,
        Production,
        Quality,
        HR,
        Supplies,
        Transport,
        Sales
    }
    public enum EventStatus { Executing, Stopped, Finished, Aborted};

    public enum EventTypes { UpgradeDepartment, HireEmployee };

    public enum NpcTypes { Customer, Supplier};

    public enum SegmentTypes { DistributionCenter, Commerce, Industry };

}
