﻿using AOB.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AOB.Core.Services.Factories
{
    public static class FactoriesGenerator<T> where T: BaseObject
    {

        public static IFactory<T> Get()
        {
            if (typeof(T) == typeof(GameEvent))
                return (IFactory<T>)new GameEventsFactory();
            else if (typeof(T) == typeof(Npc))
                return (IFactory<T>)new NpcsFactory();
            else if (typeof(T) == typeof(Commodity))
                return (IFactory<T>)new CommoditiesFactory();
            else
                return new BaseObjectFactory<T>();
        }

    }
}
