﻿using Alisson.Core.Factories;
using Alisson.Core.Models;
using Alisson.Core.Models.Commodities;
using Alisson.Core.Models.GameEvents;
using Alisson.Core.Models.Npcs;
using Alisson.Core.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alisson.Core.Services.Factories
{
    public class NpcsFactory : IFactory<Npc>
    {
        public Npc create(Dictionary<string, object> attr)
        {
            if (attr.ContainsKey("type"))
            {
                int type = Convert.ToInt32(attr["type"].ToString());
                return create((NpcTypes)type);
            }
            throw new ArgumentException("Type argument is necessary to be used in CommoditiesFactory");
        }

        public static Npc create(NpcTypes type)
        {
            switch (type)
            {
                case NpcTypes.Customer: return new Customer();
                case NpcTypes.Supplier: return new Supplier();
                default: throw new ArgumentOutOfRangeException("Invalid Commodity type!");
            }
        }

    }
}
