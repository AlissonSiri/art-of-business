﻿using Alisson.Core.Models;
using Alisson.Core.Models.Commodities;
using Alisson.Core.Models.GameEvents;
using Alisson.Core.Models.Npcs;
using Alisson.Core.Repository;
using Alisson.Core.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alisson.Core.Services.Factories
{
    public class MapObjectsFactory
    {

        public static IMapObject create(ObjectTypes objType, int objID)
        {
            switch (objType)
            {
                case ObjectTypes.Company: return BaseRepository<Company>.getByID(objID);
                case ObjectTypes.Customer:
                case ObjectTypes.Supplier: return BaseRepository<Npc>.getByID(objID);
                case ObjectTypes.EmptyMapObject: return new EmptyMapObject(objID);
                default: throw new ArgumentOutOfRangeException("Invalid Map Object type!");
            }
        }

    }
}
