﻿using Alisson.Core.Factories;
using Alisson.Core.Models;
using Alisson.Core.Models.Commodities;
using Alisson.Core.Models.GameEvents;
using Alisson.Core.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alisson.Core.Services.Factories
{
    public class CommoditiesFactory : IFactory<Commodity>
    {
        public Commodity create(Dictionary<string, object> attr)
        {
            if (attr.ContainsKey("commodity_type"))
            {
                int type = Convert.ToInt32(attr["commodity_type"].ToString());
                return create((CommodityType)type);
            }
            throw new ArgumentException("Type argument is necessary to be used in CommoditiesFactory");
        }

        public static Commodity create(CommodityType type)
        {
            switch (type)
            {
                case CommodityType.Product: return new Product();
                case CommodityType.RawMaterial: return new RawMaterial();
                default: throw new ArgumentOutOfRangeException("Invalid Commodity type!");
            }
        }

    }
}
