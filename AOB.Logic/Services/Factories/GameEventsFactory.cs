﻿using Alisson.Core.Factories;
using Alisson.Core.Models;
using Alisson.Core.Models.GameEvents;
using Alisson.Core.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alisson.Core.Services.Factories
{
    public class GameEventsFactory : IFactory<GameEvent>
    {
        public GameEvent create(Dictionary<string, object> attr)
        {
            if (attr.ContainsKey("type"))
            {
                int type = Convert.ToInt32(attr["type"].ToString());
                return create((EventTypes)type);
            }
            throw new ArgumentException("Type argument is necessary to be used in GameEventsFactory");
        }

        public static GameEvent create(EventTypes type)
        {
            switch (type)
            {
                case EventTypes.UpgradeDepartment: return new UpgradeDepartmentGameEvent();
                case EventTypes.HireEmployee: return new HireEmployeeGameEvent();
                default: throw new ArgumentOutOfRangeException("Invalid GameEvent type!");
            }
        }

    }
}
