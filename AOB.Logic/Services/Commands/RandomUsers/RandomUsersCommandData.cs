﻿using Alisson.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alisson.Core.Services.Commands.RandomUsers
{
    public class RandomUsersCommandData
    {
        public int Quantity { get; set; }

        public RandomUsersCommandData(int quantity)
        {
            this.Quantity = quantity;
        }
    }
}
