﻿using Alisson.Core.IoC;
using Alisson.Core.Models;
using Alisson.Core.Models.Commodities;
using Alisson.Core.Repository;
using Alisson.Core.Services.Commands.GenerateNeighbors;
using Alisson.Core.Services.Commands.IncreaseMap;
using Alisson.Core.Services.Commands.RegisterUser;
using Alisson.Core.Types;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Alisson.Core.Services.Commands.RandomUsers
{
    public class RandomUsersCommandHandler : ICommandHandler<RandomUsersCommandData>
    {

        private ICommandHandler<RegisterUserCommandData> reg;

        public RandomUsersCommandHandler()
        {
            this.reg = new RegisterUserCommandHandler(new GenerateNeighborsCommandHandler());
        }

        public virtual RandomUsersCommandData Handle(RandomUsersCommandData command)
        {
            string userName = "TestUser";
            string compName = "TestCompany";
            for (int i = 0; i < command.Quantity; i++)
            {
                List<CompanyType> types = CompanyType.getIndustries().ToList();
                CompanyType type = Helpers.GetRandom<CompanyType>(types);
                reg.Handle(new RegisterUserCommandData(userName + Convert.ToString(i + 1), compName + Convert.ToString(i + 1), "password", type.ID, Helpers.GetLocalIP()));
            }
                return command;
        }

    }
}
