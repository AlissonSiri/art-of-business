﻿using Alisson.Core.IoC;
using Alisson.Core.Models;
using Alisson.Core.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alisson.Core.Services.Commands.IncreaseMap
{
    public class IncreaseMapCommandHandler : ICommandHandler<IncreaseMapCommandData>
    {

        public IncreaseMapCommandHandler() { }

        public virtual IncreaseMapCommandData Handle(IncreaseMapCommandData command)
        {
            List<MapPoint> result = new List<MapPoint>();
            int start = 2;
            if (BaseRepository<MapPoint>.getAll().Count() > 0)
            {
                start = BaseRepository<MapPoint>.getAll().Max(m => m.x) + 1; // Since the map is a square, max x is also max y.
            }
            else
                result.Add(new MapPoint(1, 1));
            // Suppose currently we have 5x5 map and wanna increase 2

            // Y
            // 5  # # # # #
            // 4  # # # # #
            // 3  # # # # #
            // 2  # # # # #
            // 1  # # # # #
            //    1 2 3 4 5 X

            // We start building right side of map, from (6x,1y) up to (7x,5y)
            // Starting x with 6, until x reaches 7
            for (int xRight = start; xRight < start + command.Radius; xRight++)
            {
                for (int yRight = 1; yRight < start; yRight++)
                    result.Add(new MapPoint(xRight, yRight));
            }

            // Y
            // 5  # # # # # O O
            // 4  # # # # # O O
            // 3  # # # # # O O
            // 2  # # # # # O O
            // 1  # # # # # O O
            //    1 2 3 4 5 6 7 X

            // Then we'll build the remaining top part, from (1x,6y) up to (7x,7y)
            for (int yTop = start; yTop < start + command.Radius; yTop++)
            {
                for (int xTop = 1; xTop < start + command.Radius; xTop++)
                    result.Add(new MapPoint(xTop, yTop));
            }

            BaseRepository<MapPoint>.Submit(result);

            return command;

            // Y
            // 7  O O O O O O O 
            // 6  O O O O O O O 
            // 5  # # # # # # # 
            // 4  # # # # # # # 
            // 3  # # # # # # # 
            // 2  # # # # # # # 
            // 1  # # # # # # # 
            //    1 2 3 4 5 6 7 X

        }
    }
}
