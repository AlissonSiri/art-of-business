﻿using Alisson.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Alisson.Core.Services.Commands.GenerateNeighbors
{
    public class GenerateNeighborsCommandData
    {
        public Company Company;
        public string errorMessage = "";

        public GenerateNeighborsCommandData(Company c)
        {
            this.Company = c;
        }
    }
}
