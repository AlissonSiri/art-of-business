﻿using Alisson.Core.IoC;
using Alisson.Core.Models;
using Alisson.Core.Models.Commodities;
using Alisson.Core.Models.GameEvents;
using Alisson.Core.Models.Npcs;
using Alisson.Core.Repository;
using Alisson.Core.Services;
using Alisson.Core.Services.Commands.IncreaseMap;
using Alisson.Core.Services.Factories;
using Alisson.Core.Types;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Alisson.Core.Services.Commands.GenerateNeighbors
{
    public class GenerateNeighborsCommandHandler : ICommandHandler<GenerateNeighborsCommandData>
    {

        public GenerateNeighborsCommandHandler() { }

        public virtual GenerateNeighborsCommandData Handle(GenerateNeighborsCommandData command)
        {
            try
            {
                // Pick a suitable MapPoint for new user
                List<MapPoint> places = MapPoint.GetAvailablePlaces(command.Company).ToList();
                if (places.Count() == 0)
                {
                    new IncreaseMapCommandHandler().Handle(new IncreaseMapCommandData() { Radius = 10 });
                    places = MapPoint.GetAvailablePlaces(command.Company).ToList();
                }
                MapPoint companyPlace = Helpers.GetRandom<MapPoint>(places);
                companyPlace.setObject(command.Company);
                // Generate Customers
                List<MapPoint> neighborhoodCust = companyPlace.GetNeighbors(Customer.PlayerRadiusSearch).ToList();
                int customersRemaining = Customer.QuantityPerPlayer - neighborhoodCust.Where(n => n.getObjectType() == ObjectTypes.Customer).Count();
                this.generateNpcs(customersRemaining, NpcTypes.Customer, command.Company, neighborhoodCust);
                // Generate Suppliers
                List<MapPoint> neighborhoodSup = companyPlace.GetNeighbors(Supplier.PlayerRadiusSearch).ToList();
                int suppliersRemaining = Supplier.QuantityPerPlayer - neighborhoodSup.Where(n => n.getObjectType() == ObjectTypes.Supplier).Count();
                this.generateNpcs(suppliersRemaining, NpcTypes.Supplier, command.Company, neighborhoodSup);
                BaseRepository<MapPoint>.Submit();
                BaseRepository<Npc>.Submit();
                return command;
            }
            catch (GameEventException e)
            {
                command.errorMessage = e.Message;
                return command;
            }
        }

        private void generateNpcs(int quantity, NpcTypes type, Company comp, List<MapPoint> neighborhood)
        {
            List<Commodity> starterCommodities = comp.getStarterCommodities(Npc.DiscoverCommodityType(type)).ToList();
            for (int i = 0; i < quantity; i++)
            {
                Npc npc = NpcsFactory.create(type);
                List<MapPoint> availablePlaces = MapPoint.GetAvailablePlaces(npc, neighborhood).ToList(); // Check in this neighborhood available places for this new NPC
                if (availablePlaces.Count > 0) // If the new player has no luck, they will get NPCs "a bit farther" than expected.
                {
                    BaseRepository<Npc>.add(npc); // We only submit to repository if we are sure there is a place for this NPC!
                    this.DefineCommodities(npc, availablePlaces);
                }

            }
            // If we are very unlucky and there is no NPC for trading at least one of our company's starter commodities, we force its trading in its nearest NPC. (this is very unlike to happen).
            if (BaseRepository<NpcCommodity>.Count(nc => starterCommodities.Select(sc => sc.ID).Contains(nc.Commodity)) == 0)
            {
                int npcID = comp.GetNearestNpc(type, neighborhood).ID;
                int commID = Helpers.GetRandom<Commodity>(starterCommodities).ID;
                BaseRepository<NpcCommodity>.add(new NpcCommodity() { Npc = npcID, Commodity = commID });
            }
                
        }

        private void DefineCommodities(Npc npc, List<MapPoint> neighborhoodAvailablePlaces)
        {
                MapPoint customerPlace = Helpers.GetRandom<MapPoint>(neighborhoodAvailablePlaces);
                customerPlace.setObject(npc);
                List<int> companyIds = customerPlace.GetNeighbors(npc.GetPlayerRadiusSearch()).Where(n => n.ObjectType == (int)ObjectTypes.Company).Select(mp => mp.ObjectId).ToList(); // Here we'll get all companies IDs nearby.
                List<Company> neighborCompanies = BaseRepository<Company>.getAll(c => companyIds.Contains(c.ID)).ToList(); // And with the ID List, we can get the Company List.
                List<Commodity> comm = neighborCompanies.SelectMany(c => c.getCommodities(npc.GetCommodityType())).ToList(); // For each company, get an ID List of its commodities. The SelectMany will merge the ID lists into a unique distinct list.
                List<int> comms = comm.Select(c => c.ID).ToList(); 
                int commsQtt = Math.Min(comms.Count, npc.GetTradingCommoditiesQuantity()); // If we have more commodities in neighborhood than necessary, lets use just the necessary. Otherwise, let's use what we have.
                // Lets fill npc's with random neighborhood commodities.
                for (int q = 0; q < commsQtt; q++)
                    BaseRepository<NpcCommodity>.add(new NpcCommodity() { Npc = npc.ID, Commodity = Helpers.GetRandom<int>(comms, npc.getCommodities().Select(c => c.ID).ToList()) });
                List<Commodity> commodities = BaseRepository<Commodity>.getAll().ToList();
                // If we couldn't fill npc's commodities because there was a lack of commodities nearby, we choose the remaining ones randomly.
                while (npc.GetTradingCommoditiesQuantity() - npc.getCommodities().Count() != 0)
                    BaseRepository<NpcCommodity>.add(new NpcCommodity() { Npc = npc.ID, Commodity = Helpers.GetRandom<Commodity>(commodities).ID });
        }

    }
}
