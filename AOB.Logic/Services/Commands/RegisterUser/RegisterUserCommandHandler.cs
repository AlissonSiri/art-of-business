﻿using Alisson.Core.IoC;
using Alisson.Core.Models;
using Alisson.Core.Models.Commodities;
using Alisson.Core.Repository;
using Alisson.Core.Services.Commands.GenerateNeighbors;
using Alisson.Core.Services.Commands.IncreaseMap;
using Alisson.Core.Types;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Alisson.Core.Services.Commands.RegisterUser
{
    public class RegisterUserCommandHandler : ICommandHandler<RegisterUserCommandData>
    {

        private ICommandHandler<GenerateNeighborsCommandData> genNeighbors;

        public RegisterUserCommandHandler(ICommandHandler<GenerateNeighborsCommandData> genNeighbors)
        {
            this.genNeighbors = genNeighbors;
        }

        public virtual RegisterUserCommandData Handle(RegisterUserCommandData command)
        {
            User user = new User() { Name = command.UserName, CompanyName = command.CompanyName, Password = command.Password };
            BaseRepository<User>.add(user);
            UserIp ip = new UserIp();
            ip.UserId = user.ID;
            ip.Ip = command.UserIP;
            ip.save();
            // Generate company with default config
            Company comp = new Company();
            comp.BranchNick = user.CompanyName;
            comp.CurrentMoney = GlobalVariables.START_MONEY;
            comp.Type = command.CompanyType;
            comp.User = user.ID;
            BaseRepository<Company>.add(comp);
            comp.createBaseDepartments();
            this.genNeighbors.Handle(new GenerateNeighborsCommandData(comp));
            command.UserID = user.ID;
            return command;
        }

    }
}
