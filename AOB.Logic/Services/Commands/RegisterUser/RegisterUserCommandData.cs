﻿using Alisson.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alisson.Core.Services.Commands.RegisterUser
{
    public class RegisterUserCommandData
    {
        public string CompanyName { get; set; }
        public int CompanyType { get; set; }
        public string Password { get; set; }
        public int UserID { get; set; }
        public string UserIP { get; set; }
        public string UserName { get; set; }

        public RegisterUserCommandData(string name, string companyName, string password, int companyType, string IP)
        {
            this.CompanyType = companyType;
            this.UserName = name;
            this.CompanyName = companyName;
            this.Password = password;
            this.UserIP = IP;
        }
    }
}
