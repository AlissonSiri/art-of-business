﻿using Alisson.Core.IoC;
using Alisson.Core.Models;
using Alisson.Core.Models.GameEvents;
using Alisson.Core.Repository;
using Alisson.Core.Services;
using System.Linq;

namespace Alisson.Core.Services.Commands.HireEmployee
{
    public class HireEmployeeCommandHandler : ICommandHandler<HireEmployeeCommandData>
    {

        public HireEmployeeCommandHandler() { }

        public virtual HireEmployeeCommandData Handle(HireEmployeeCommandData command)
        {
            try
            {
                Department d = command.department;
                HireEmployeeGameEvent ev = new HireEmployeeGameEvent(command.quantity, d);
                BaseRepository< GameEvent>.add(ev); // Create new event
                return command;
            }
            catch (GameEventException e)
            {
                command.errorMessage = e.Message;
                return command;
            }
        }
    }
}
