﻿using Alisson.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Alisson.Core.Services.Commands.HireEmployee
{
    public class HireEmployeeCommandData
    {
        public Department department;
        public string errorMessage = "";
        public int quantity;

        public HireEmployeeCommandData(int qtt, Department d)
        {
            this.quantity = qtt;
            this.department = d;
        }
    }
}
