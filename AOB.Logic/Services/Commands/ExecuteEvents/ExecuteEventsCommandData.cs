﻿using Alisson.Core.Models;
using System.Collections.Generic;

namespace Alisson.Core.Services.Commands.ExecuteEvents
{
    public class ExecuteEventsCommandData
    {

        public int user { get; private set; }

        public ExecuteEventsCommandData(int userID)
        {
            this.user = userID;
        }

    }
}
