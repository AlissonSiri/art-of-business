﻿using Alisson.Core.IoC;
using Alisson.Core.Models;
using Alisson.Core.Repository;
using Alisson.Core.Services;
using Alisson.Core.Types;
using System.Collections.Generic;
using System.Linq;

namespace Alisson.Core.Services.Commands.ExecuteEvents
{
    public class ExecuteEventsCommandHandler : ICommandHandler<ExecuteEventsCommandData>
    {


        public ExecuteEventsCommandHandler() { }

        public virtual ExecuteEventsCommandData Handle(ExecuteEventsCommandData command)
        {
            IEnumerable<Company> comps = BaseRepository<Company>.getAll().Where(c => c.User == command.user);
            IEnumerable<GameEvent> evts = BaseRepository<GameEvent>.getAll().Where(ev => ev.getStatus() == EventStatus.Executing && comps.Any(c => c.User == command.user) && ev.HasPassedFinishDate());
            List<GameEvent> result = new List<GameEvent>();
            while (evts.Count() > 0)
            {
                // Only executing events. Stopped events won't be executed here, but from a Subject event.
                GameEvent e = evts.OrderBy(g => g.FinishDate).First();
                try
                {
                    e.finish();
                    result.Add(e);
                }
                catch (GameEventException)
                {
                }
                evts = BaseRepository<GameEvent>.getAll().Where(ev => ev.getStatus() == EventStatus.Executing && comps.Any(c => c.User == command.user) && ev.HasPassedFinishDate());
            }
            BaseRepository<GameEvent>.Submit(result); // Save all events to database
            return command;
        }
    }
}
