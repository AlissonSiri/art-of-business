﻿using Alisson.Core.IoC;
using Alisson.Core.Models;
using Alisson.Core.Models.GameEvents;
using Alisson.Core.Repository;
using Alisson.Core.Services;
using Alisson.Core.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alisson.Core.Services.Commands.UpgradeDepartment
{
    public class UpgradeDepartmentCommandHandler : ICommandHandler<UpgradeDepartmentCommandData>
    {

        public UpgradeDepartmentCommandHandler() { }

        public virtual UpgradeDepartmentCommandData Handle(UpgradeDepartmentCommandData command)
        {
            try
            {
                Department d = command.department;
                UpgradeDepartmentGameEvent ev = new UpgradeDepartmentGameEvent(d);
                BaseRepository<GameEvent>.add(ev); // Create new event
                return command;
            }
            catch (GameEventException e)
            {
                command.errorMessage = e.Message;
                return command;
            }
        }
    }
}
