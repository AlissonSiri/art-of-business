﻿using Alisson.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alisson.Core.Services.Commands.UpgradeDepartment
{
    public class UpgradeDepartmentCommandData
    {
        public Department department { get; set; }
        public string errorMessage = "";

        public UpgradeDepartmentCommandData(Department d)
        {
            this.department = d;
        }

    }
}
