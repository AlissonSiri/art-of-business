﻿using Alisson.Core.IoC;
using Alisson.Core.Models;
using Alisson.Core.Models.Commodities;
using Alisson.Core.Repository;
using Alisson.Core.Services.Commands.ExecuteEvents;
using Alisson.Core.Services.Commands.GenerateNeighbors;
using Alisson.Core.Services.Commands.HireEmployee;
using Alisson.Core.Services.Commands.RegisterUser;
using Alisson.Core.Services.Commands.UpgradeDepartment;
using Alisson.Core.Types;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AOB.Logic.Config
{
    public class Config
    {

        public static Action<Container> config = ConfigureFactories;
        public static Action loadData = LoadInitialMemoryData;

        private static void ConfigureFactories(Container container)
        {
            GenericFactory<UpgradeDepartmentCommandHandler>.CreationClosure = () => container.GetInstance(typeof(UpgradeDepartmentCommandHandler));
            GenericFactory<ExecuteEventsCommandHandler>.CreationClosure = () => container.GetInstance(typeof(ExecuteEventsCommandHandler));
            GenericFactory<HireEmployeeCommandHandler>.CreationClosure = () => container.GetInstance(typeof(HireEmployeeCommandHandler));
        }

        public static void LoadInitialMemoryData()
        {

            // Departments
            BaseRepository<DepartmentType>.add(new DepartmentType() { Name = "Atendimento", Description = "Teste", MaxLevel = 10, MultiplicationFactor = 1, Start = true, StartingEmployees = 2 });
            BaseRepository<DepartmentType>.add(new DepartmentType() { Name = "Construção Civil", Description = "Teste", MaxLevel = 10, MultiplicationFactor = 1, Start = true, StartingEmployees = 2 });
            BaseRepository<DepartmentType>.add(new DepartmentType() { Name = "Contabilidade", Description = "Teste", MaxLevel = 10, MultiplicationFactor = 1, Start = true, StartingEmployees = 2 });
            BaseRepository<DepartmentType>.add(new DepartmentType() { Name = "Estoque", Description = "Teste", MaxLevel = 10, MultiplicationFactor = 1, Start = true, StartingEmployees = 2 });
            BaseRepository<DepartmentType>.add(new DepartmentType() { Name = "Laboratório", Description = "Teste", MaxLevel = 10, MultiplicationFactor = 1, Start = false, StartingEmployees = 2 });
            BaseRepository<DepartmentType>.add(new DepartmentType() { Name = "Produção", Description = "Teste", MaxLevel = 10, MultiplicationFactor = 1, Start = true, StartingEmployees = 2 });
            BaseRepository<DepartmentType>.add(new DepartmentType() { Name = "Qualidade", Description = "Teste", MaxLevel = 10, MultiplicationFactor = 1, Start = false, StartingEmployees = 2 });
            BaseRepository<DepartmentType>.add(new DepartmentType() { Name = "Recursos Humanos", Description = "Teste", MaxLevel = 10, MultiplicationFactor = 1, Start = true, StartingEmployees = 2 });
            BaseRepository<DepartmentType>.add(new DepartmentType() { Name = "Suprimentos", Description = "Teste", MaxLevel = 10, MultiplicationFactor = 1, Start = true, StartingEmployees = 2 });
            BaseRepository<DepartmentType>.add(new DepartmentType() { Name = "Transporte", Description = "Teste", MaxLevel = 10, MultiplicationFactor = 1, Start = true, StartingEmployees = 2 });
            BaseRepository<DepartmentType>.add(new DepartmentType() { Name = "Vendas", Description = "Teste", MaxLevel = 10, MultiplicationFactor = 1, Start = true, StartingEmployees = 2 });

            // Products
            BaseRepository<Commodity>.add(new Product() { Name = "Cadeira", CommodityType = (int)CommodityType.Product, Description = "Teste", m3 = 20, Start = true, StartPrice = 400, Weight = 20 });
            BaseRepository<Commodity>.add(new Product() { Name = "Mesa", CommodityType = (int)CommodityType.Product, Description = "Teste", m3 = 70, Start = false, StartPrice = 800, Weight = 100 });
            BaseRepository<Commodity>.add(new Product() { Name = "Guarda-Roupas", CommodityType = (int)CommodityType.Product, Description = "Teste", m3 = 120, Start = false, StartPrice = 1500, Weight = 200 });
            BaseRepository<Commodity>.add(new Product() { Name = "Cama", CommodityType = (int)CommodityType.Product, Description = "Teste", m3 = 120, Start = false, StartPrice = 1500, Weight = 200 });

            BaseRepository<Commodity>.add(new Product() { Name = "Carro Popular", CommodityType = (int)CommodityType.Product, Description = "Teste", m3 = 120, Start = true, StartPrice = 1500, Weight = 200 });
            BaseRepository<Commodity>.add(new Product() { Name = "Pick Up", CommodityType = (int)CommodityType.Product, Description = "Teste", m3 = 120, Start = false, StartPrice = 1500, Weight = 200 });
            BaseRepository<Commodity>.add(new Product() { Name = "Truck", CommodityType = (int)CommodityType.Product, Description = "Teste", m3 = 120, Start = false, StartPrice = 1500, Weight = 200 });
            BaseRepository<Commodity>.add(new Product() { Name = "Baú", CommodityType = (int)CommodityType.Product, Description = "Teste", m3 = 120, Start = false, StartPrice = 1500, Weight = 200 });

            BaseRepository<Commodity>.add(new Product() { Name = "Roupa Casual", CommodityType = (int)CommodityType.Product, Description = "Teste", m3 = 120, Start = true, StartPrice = 1500, Weight = 200 });
            BaseRepository<Commodity>.add(new Product() { Name = "Roupa de Cama", CommodityType = (int)CommodityType.Product, Description = "Teste", m3 = 120, Start = false, StartPrice = 1500, Weight = 200 });
            BaseRepository<Commodity>.add(new Product() { Name = "Uniforme", CommodityType = (int)CommodityType.Product, Description = "Teste", m3 = 120, Start = false, StartPrice = 1500, Weight = 200 });
            BaseRepository<Commodity>.add(new Product() { Name = "EPI", CommodityType = (int)CommodityType.Product, Description = "Teste", m3 = 120, Start = false, StartPrice = 1500, Weight = 200 });

            // Raw Material -- starts in 13
            BaseRepository<Commodity>.add(new RawMaterial() { Name = "Madeira", CommodityType = (int)CommodityType.RawMaterial, Description = "Teste", m3 = 20, Start = true, StartPrice = 400, Weight = 20 });
            BaseRepository<Commodity>.add(new RawMaterial() { Name = "Metal", CommodityType = (int)CommodityType.RawMaterial, Description = "Teste", m3 = 70, Start = true, StartPrice = 800, Weight = 100 });
            BaseRepository<Commodity>.add(new RawMaterial() { Name = "Vidro", CommodityType = (int)CommodityType.RawMaterial, Description = "Teste", m3 = 120, Start = true, StartPrice = 1500, Weight = 200 });
            BaseRepository<Commodity>.add(new RawMaterial() { Name = "Algodão", CommodityType = (int)CommodityType.RawMaterial, Description = "Teste", m3 = 120, Start = true, StartPrice = 1500, Weight = 200 });
            BaseRepository<Commodity>.add(new RawMaterial() { Name = "Couro", CommodityType = (int)CommodityType.RawMaterial, Description = "Teste", m3 = 120, Start = true, StartPrice = 1500, Weight = 200 });
            BaseRepository<Commodity>.add(new RawMaterial() { Name = "Borracha", CommodityType = (int)CommodityType.RawMaterial, Description = "Teste", m3 = 120, Start = true, StartPrice = 1500, Weight = 200 });
            BaseRepository<Commodity>.add(new RawMaterial() { Name = "Plástico", CommodityType = (int)CommodityType.RawMaterial, Description = "Teste", m3 = 120, Start = true, StartPrice = 1500, Weight = 200 });

            // Product x Raw Material relationship
            BaseRepository<ProductRawMaterial>.add(new ProductRawMaterial() { Product = 1, RawMaterial = 13, Quantity = 5 });
            BaseRepository<ProductRawMaterial>.add(new ProductRawMaterial() { Product = 2, RawMaterial = 13, Quantity = 20 });
            BaseRepository<ProductRawMaterial>.add(new ProductRawMaterial() { Product = 2, RawMaterial = 14, Quantity = 7 });
            BaseRepository<ProductRawMaterial>.add(new ProductRawMaterial() { Product = 3, RawMaterial = 13, Quantity = 80 });
            BaseRepository<ProductRawMaterial>.add(new ProductRawMaterial() { Product = 3, RawMaterial = 15, Quantity = 15 });
            BaseRepository<ProductRawMaterial>.add(new ProductRawMaterial() { Product = 4, RawMaterial = 13, Quantity = 3 });
            BaseRepository<ProductRawMaterial>.add(new ProductRawMaterial() { Product = 4, RawMaterial = 14, Quantity = 3 });
            BaseRepository<ProductRawMaterial>.add(new ProductRawMaterial() { Product = 4, RawMaterial = 15, Quantity = 3 });

            BaseRepository<ProductRawMaterial>.add(new ProductRawMaterial() { Product = 5, RawMaterial = 16, Quantity = 3 });
            BaseRepository<ProductRawMaterial>.add(new ProductRawMaterial() { Product = 5, RawMaterial = 17, Quantity = 3 });
            BaseRepository<ProductRawMaterial>.add(new ProductRawMaterial() { Product = 5, RawMaterial = 18, Quantity = 3 });
            BaseRepository<ProductRawMaterial>.add(new ProductRawMaterial() { Product = 6, RawMaterial = 16, Quantity = 3 });
            BaseRepository<ProductRawMaterial>.add(new ProductRawMaterial() { Product = 6, RawMaterial = 17, Quantity = 3 });
            BaseRepository<ProductRawMaterial>.add(new ProductRawMaterial() { Product = 6, RawMaterial = 18, Quantity = 3 });
            BaseRepository<ProductRawMaterial>.add(new ProductRawMaterial() { Product = 7, RawMaterial = 16, Quantity = 3 });
            BaseRepository<ProductRawMaterial>.add(new ProductRawMaterial() { Product = 7, RawMaterial = 17, Quantity = 3 });
            BaseRepository<ProductRawMaterial>.add(new ProductRawMaterial() { Product = 7, RawMaterial = 18, Quantity = 3 });
            BaseRepository<ProductRawMaterial>.add(new ProductRawMaterial() { Product = 7, RawMaterial = 16, Quantity = 3 });
            BaseRepository<ProductRawMaterial>.add(new ProductRawMaterial() { Product = 7, RawMaterial = 17, Quantity = 3 });
            BaseRepository<ProductRawMaterial>.add(new ProductRawMaterial() { Product = 7, RawMaterial = 18, Quantity = 3 });

            BaseRepository<ProductRawMaterial>.add(new ProductRawMaterial() { Product = 9, RawMaterial = 16, Quantity = 3 });
            BaseRepository<ProductRawMaterial>.add(new ProductRawMaterial() { Product = 10, RawMaterial = 16, Quantity = 3 });
            BaseRepository<ProductRawMaterial>.add(new ProductRawMaterial() { Product = 11, RawMaterial = 16, Quantity = 3 });
            BaseRepository<ProductRawMaterial>.add(new ProductRawMaterial() { Product = 11, RawMaterial = 17, Quantity = 3 });
            BaseRepository<ProductRawMaterial>.add(new ProductRawMaterial() { Product = 12, RawMaterial = 16, Quantity = 3 });
            BaseRepository<ProductRawMaterial>.add(new ProductRawMaterial() { Product = 12, RawMaterial = 17, Quantity = 3 });
            BaseRepository<ProductRawMaterial>.add(new ProductRawMaterial() { Product = 12, RawMaterial = 19, Quantity = 3 });

            startCompanies();

            GenerateNeighborsCommandHandler neg = new GenerateNeighborsCommandHandler();

            RegisterUserCommandData data = new RegisterUserCommandData("Alisson", "AOB", "123", 3, "0.0.0.0");

            RegisterUserCommandHandler reg = new RegisterUserCommandHandler(neg);
            int userID = reg.Handle(data).UserID;

        }

        private static void startCompanies()
        {

            int ctID;

            // Company Types
            ctID = BaseRepository<CompanyType>.add(new CompanyType() { Description = "Fábrica carros...", Name = "Automobilística", Status = true, SegmentType = (int)SegmentTypes.Industry }).ID;

            BaseRepository<CompanyTypeProducts>.add(new CompanyTypeProducts() { Product = 5, CompanyType = ctID });
            BaseRepository<CompanyTypeProducts>.add(new CompanyTypeProducts() { Product = 6, CompanyType = ctID });
            BaseRepository<CompanyTypeProducts>.add(new CompanyTypeProducts() { Product = 7, CompanyType = ctID });
            BaseRepository<CompanyTypeProducts>.add(new CompanyTypeProducts() { Product = 8, CompanyType = ctID });

            //ctID = BaseRepository<CompanyType>.add(new CompanyType() { Description = "Fábrica móveis", Name = "Petroleira", Status = true, SegmentType = (int)SegmentTypes.Industry }).ID;

            ctID = BaseRepository<CompanyType>.add(new CompanyType() { Description = "Fábrica móveis...", Name = "Moveleira", Status = true, SegmentType = (int)SegmentTypes.Industry }).ID;

            BaseRepository<CompanyTypeProducts>.add(new CompanyTypeProducts() { Product = 1, CompanyType = ctID });
            BaseRepository<CompanyTypeProducts>.add(new CompanyTypeProducts() { Product = 2, CompanyType = ctID });
            BaseRepository<CompanyTypeProducts>.add(new CompanyTypeProducts() { Product = 3, CompanyType = ctID });
            BaseRepository<CompanyTypeProducts>.add(new CompanyTypeProducts() { Product = 4, CompanyType = ctID });

            //ctID = BaseRepository<CompanyType>.add(new CompanyType() { Description = "Fábrica móveis", Name = "de Eletrônicos", Status = true, SegmentType = (int)SegmentTypes.Industry }).ID;

            ctID = BaseRepository<CompanyType>.add(new CompanyType() { Description = "Confecciona roupas...", Name = "Têxtil", Status = true, SegmentType = (int)SegmentTypes.Industry }).ID;

            BaseRepository<CompanyTypeProducts>.add(new CompanyTypeProducts() { Product = 9, CompanyType = ctID });
            BaseRepository<CompanyTypeProducts>.add(new CompanyTypeProducts() { Product = 10, CompanyType = ctID });
            BaseRepository<CompanyTypeProducts>.add(new CompanyTypeProducts() { Product = 11, CompanyType = ctID });
            BaseRepository<CompanyTypeProducts>.add(new CompanyTypeProducts() { Product = 12, CompanyType = ctID });

            //ctID = BaseRepository<CompanyType>.add(new CompanyType() { Description = "Fábrica móveis", Name = "Alimentícia", Status = true, SegmentType = (int)SegmentTypes.Industry }).ID;

        }

    }
}
