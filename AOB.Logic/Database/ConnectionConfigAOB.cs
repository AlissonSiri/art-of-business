﻿using Alisson.Core.Database.Connections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alisson.Core.Database
{
    public class ConnectionConfigAOB: ConnectionConfig
    {

        protected static string databaseName = "aob";
        protected static string password = "a2xl67*bxY";
        protected static string serverName = "localhost";
        protected static string userID = "Alisson";

        public static void configure()
        {
            config(new MemoryConn(), databaseName, password, serverName, userID).Start();
        }
    }
}
