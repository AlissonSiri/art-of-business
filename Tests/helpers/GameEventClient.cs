﻿using Alisson.Core.IoC;
using Alisson.Core.Models;
using Alisson.Core.Services.Commands.ExecuteEvents;
using Alisson.Core.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tests.forms.core;

namespace Tests.helpers
{
    public class GameEventClient
    {

        public int ID { get; set; }
        private GameEvent ev { get; set; }
        private DateTime FinishDate { get; set; }
        private IGameInterface Form { get; set; }

        public GameEventClient(GameEvent ev, IGameInterface form)
        {
            this.ID = ev.ID;
            this.ev = ev;
            this.FinishDate = ev.FinishDate;
            this.Form = form;
        }

        public TimeSpan getCurrentRemainingTime()
        {
            if (DateTime.UtcNow > this.FinishDate) // First we check if FinishDate has passed
            {
                // Normally we'd pass the GameEvent ID to post, and post would do this code, since the client doesn't know the commanders, IoC Factories, etc.
                // We try to stop only if it's executing. If it can't finish, it'll be stopped and once stopped, we won't want to try to finish every time, we'll just wait for an update by other event.
                if (this.ev.getStatus() == EventStatus.Executing)
                {
                    ExecuteEventsCommandData data = new ExecuteEventsCommandData(Session.UserID);
                    ExecuteEventsCommandHandler h = GenericFactory<ExecuteEventsCommandHandler>.GetDefault();
                    h.Handle(data);
                    FormHelpers.RefreshScreens();
                }
                return new TimeSpan(0); // Actually this line should never be reached.
            }
            return TimeSpan.FromSeconds(Math.Round((this.FinishDate - DateTime.UtcNow).TotalSeconds));

        }

        public override string ToString()
        {
            return String.Format("{0}. Tempo restante: {1}", this.ev.GetDescription(), this.getCurrentRemainingTime());
        }

    }
}
