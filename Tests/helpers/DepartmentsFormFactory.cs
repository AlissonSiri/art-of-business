﻿using Alisson.Core.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tests.forms.core;

namespace Tests.forms.departments
{
    public static class DepartmentsFormFactory
    {
        public static UserControl make(DepartmentTypes type)
        {
            switch (type)
            {
                case DepartmentTypes.CivilConstruction: return new CivilConstruction();
                case DepartmentTypes.HR: return new HR();
                default: return new UserControl();
            }
        }
    }
}
