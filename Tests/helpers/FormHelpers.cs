﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tests.forms.core;

namespace Tests.helpers
{
    public static class FormHelpers
    {

        private static List<IGameInterface> list = new List<IGameInterface>();

        public static Panel mainClient { get; set; }

        public static void PutInMainForm(UserControl u)
        {
            PutUserControl(mainClient, u);
        }

        public static void PutUserControl(Panel panel, UserControl form )
        {
            panel.Controls.Clear();
            panel.Controls.Add(form);
            form.Dock = DockStyle.Fill;
            form.Height = panel.Height;
            form.Width = panel.Width;
            form.MaximumSize = new System.Drawing.Size(panel.MaximumSize.Width, panel.MaximumSize.Height);
            form.AutoSize = true;
            form.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            if (form is IGameInterface)
                list.Add((IGameInterface)form);
        }

        public static void RefreshScreens()
        {
            foreach (IGameInterface ui in list)
                ui.RefreshScreenInfo();
        }

    }
}
