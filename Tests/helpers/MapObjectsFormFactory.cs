﻿using Alisson.Core.Models;
using Alisson.Core.Models.Npcs;
using Alisson.Core.Repository;
using Alisson.Core.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tests.forms.core;
using Tests.forms.mapobjects;

namespace Tests.forms.departments
{
    public static class MapObjectsFormFactory
    {
        public static UserControl make(IMapObject obj)
        {
            switch (obj.getObjectType())
            {
                case ObjectTypes.Company: return new ucMOCompany(obj.UnderlyingObject<Company>());
                case ObjectTypes.EmptyMapObject: return new ucMOEmptyMapObject(obj.UnderlyingObject<EmptyMapObject>());
                case ObjectTypes.Customer:
                case ObjectTypes.Supplier: return new ucMONpc(obj.UnderlyingObject<Npc>());
                default: return new UserControl();
            }
        }
    }
}
