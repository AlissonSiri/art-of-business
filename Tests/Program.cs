﻿using Alisson.Core.IoC;
using AOB.Logic.Config;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Tests
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        /// 

        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            DependencyInjectionStarter.Initialize(Config.config);
            Application.Run(DependencyInjectionStarter.container.GetInstance<frmMain>());
        }

    }
}
