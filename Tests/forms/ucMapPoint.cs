﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Alisson.Core.Models;
using Tests.helpers;
using Tests.forms.departments;
using Tests.forms.core;

namespace Tests.forms
{
    public partial class ucMapPoint : UserControl
    {

        MapPoint point;
        IMapObject obj;

        public ucMapPoint(MapPoint point)
        {
            InitializeComponent();
            this.point = point;
            this.obj = point.getMapObject();
            lblTitle.Text = point.getDisplayText();
            lblDescription.Text = obj.GetDescription();
        }

        private void ucMapPoint_Load(object sender, EventArgs e)
        {
            this.LoadObjectDetails();
        }

        private void LoadObjectDetails()
        {
            FormHelpers.PutUserControl(pnlClient, MapObjectsFormFactory.make(this.obj));
        }

    }
}
