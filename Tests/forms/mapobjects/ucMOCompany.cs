﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Alisson.Core.Models;
using Tests.forms.core;
using Alisson.Core.Services;

namespace Tests.forms.mapobjects
{
    public partial class ucMOCompany : UserControl
    {

        private Company comp;

        public ucMOCompany(Company comp)
        {
            this.comp = comp;
            InitializeComponent();
        }

        private void ucMOCompany_Load(object sender, EventArgs e)
        {
            this.LoadCompany();
        }

        private void LoadCompany()
        {
            lblCompanyName.Text = comp.BranchNick;
            lblEmployees.Text = comp.getTotalEmployees().ToString();
            lblMoney.Text = AOBHelpers.FormatToBrions(comp.CurrentMoney);
        }
    }
}
