﻿namespace Tests.forms.mapobjects
{
    partial class ucMOEmptyMapObject
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnFundar = new System.Windows.Forms.Button();
            this.lblDescription = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnFundar
            // 
            this.btnFundar.Location = new System.Drawing.Point(67, 107);
            this.btnFundar.Name = "btnFundar";
            this.btnFundar.Size = new System.Drawing.Size(75, 23);
            this.btnFundar.TabIndex = 0;
            this.btnFundar.Text = "Construir";
            this.btnFundar.UseVisualStyleBackColor = true;
            // 
            // lblDescription
            // 
            this.lblDescription.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDescription.Location = new System.Drawing.Point(0, 0);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(218, 46);
            this.lblDescription.TabIndex = 1;
            this.lblDescription.Text = "Aqui você poderá fundar uma nova filial!";
            this.lblDescription.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ucMOEmptyMapObject
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lblDescription);
            this.Controls.Add(this.btnFundar);
            this.Name = "ucMOEmptyMapObject";
            this.Size = new System.Drawing.Size(218, 149);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnFundar;
        private System.Windows.Forms.Label lblDescription;
    }
}
