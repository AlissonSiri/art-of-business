﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tests.forms.core;
using Alisson.Core.Models;
using Tests.helpers;

namespace Tests.forms.mapobjects
{
    public partial class ucMONpc : UserControl
    {
        private Npc npc;

        public ucMONpc(Npc npc)
        {
            this.npc = npc;
            InitializeComponent();
        }

        private void ucNpc_Load(object sender, EventArgs e)
        {
            this.LoadNPC();
        }

        private void LoadNPC()
        {
            lblTask.Text = npc.getTaskDescription() + ":";
            this.LoadCommodities();
        }

        private void LoadCommodities()
        {
            ttvComm.Nodes.Clear();
            foreach (Commodity comm in npc.getCommodities())
                ttvComm.Nodes.Add(comm.Name, comm.Name, comm.GetImageName());
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FormHelpers.PutInMainForm(new ucNPC(npc));
        }
    }
}
