﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Alisson.Core.Models;
using Tests.forms.core;

namespace Tests.forms.mapobjects
{
    public partial class ucMOEmptyMapObject : UserControl
    {
        private EmptyMapObject obj;

        public ucMOEmptyMapObject(EmptyMapObject obj)
        {
            this.obj = obj;
            InitializeComponent();
        }
    }
}
