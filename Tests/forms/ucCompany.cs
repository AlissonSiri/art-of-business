﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Alisson.Core.Models;
using Alisson.Core.Repository;
using Tests.helpers;
using Tests.forms.departments;
using Tests.forms.core;
using Alisson.Core.Services;

namespace Tests.forms
{
    public partial class ucCompany : UserControl, IGameInterface
    {

        private static List<GameEventClient> lstGameEvents = new List<GameEventClient>();

        public ucCompany()
        {
            InitializeComponent();
        }

        private void LoadDepartment(Department d)
        {
            try
            {
                FormHelpers.PutUserControl(this.pnlDepartment, new ucDepartment(d));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void ucCompany_Load(object sender, EventArgs e)
        {

            IEnumerable<Company> comps = BaseRepository<Company>.getAll().Where(c => c.User == Session.UserID);
            cbbCompanies.DataSource = new BindingSource(comps, null);
            cbbCompanies.DisplayMember = "BranchNick";
            cbbCompanies.ValueMember = "ID";

            if (Session.SelectedCompanyID == 0)
                Session.SelectedCompanyID = (int)cbbCompanies.SelectedValue;

            IEnumerable<Department> depts = BaseRepository<Department>.getAll().Where(d => comps.Any(c => c.ID == d.Company));
            lstDepts.DataSource = new BindingSource(depts, null);
            lstDepts.ValueMember = "ID";
            lstDepts.DisplayMember = "";

            this.LoadGameEvents();

            this.LoadCompany();

        }

        public void LoadCompany()
        {
            Company comp = BaseRepository<Company>.getAll().Where(c => c.ID == Session.SelectedCompanyID).First();
            lblCompanyName.Text = comp.BranchNick;
            lblEmployees.Text = comp.getTotalEmployees().ToString();
            lblMoney.Text = AOBHelpers.FormatToBrions(comp.CurrentMoney);
        }

        public void LoadGameEvents()
        {
            IEnumerable<GameEvent> evts = BaseRepository<GameEvent>.getAll().Where(ev => ev.Company == Session.SelectedCompanyID && ev.HasActiveStatus()).OrderBy(ev => ev.FinishDate);
            lstGameEvents.Clear();
            foreach (GameEvent evt in evts)
                lstGameEvents.Add(new GameEventClient(evt, this));
            if (lstGameEvents.Count() > 0)
            {
                lstEvents.DataSource = new BindingSource(lstGameEvents, null);
                lstEvents.ValueMember = "ID";
                lstEvents.DisplayMember = "";
            }
        }

        private void lstDepts_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.LoadDepartment((Department)lstDepts.SelectedItem);
        }

        private void lstCompanies_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Session.SelectedCompanyID = (int)cbbCompanies.SelectedValue;
                this.LoadCompany();
            }
            catch (Exception)
            {

            }
        }

        public void RefreshScreenInfo()
        {
            this.LoadCompany();
            this.LoadGameEvents();
        }

        private void cbbCompanies_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Session.SelectedCompanyID = (int)cbbCompanies.SelectedValue;
                this.LoadCompany();
            }
            catch (Exception)
            {

            }
        }

        private void tmrEvents_Tick(object sender, EventArgs e)
        {
            object lst = lstEvents.DataSource;
            lstEvents.DataSource = null;
            lstEvents.DataSource = lst;
        }

    }
}
