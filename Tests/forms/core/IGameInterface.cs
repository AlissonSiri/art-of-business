﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tests.forms.core
{
    public interface IGameInterface
    {
        string Name { get; set; }
        void RefreshScreenInfo();
    }
}
