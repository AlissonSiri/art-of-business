﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Alisson.Core.Repository;
using Alisson.Core.Models;
using Alisson.Core.Services.Commands.HireEmployee;
using Alisson.Core.IoC;
using Tests.forms.core;
using Alisson.Core.Services;
using Tests.helpers;

namespace Tests.forms.departments
{
    public partial class HR : UserControl, IGameInterface
    {

        private readonly Company c;
        private double cost;
        private TimeSpan duration;
        private int max;

        public HR()
        {
            InitializeComponent();
            this.c = BaseRepository<Company>.getAll(c => c.ID == Session.SelectedCompanyID).First();
            this.LoadDepartments();
            this.calculateDept();
        }

        private void btnHire_Click(object sender, EventArgs e)
        {
            int qtt = (int)edtQtd.Value;
            if (qtt == 0)
                return;
            Department d = (Department)cbbDepartments.SelectedItem;
            HireEmployeeCommandData data = new HireEmployeeCommandData(qtt, d);
            HireEmployeeCommandHandler h = GenericFactory<HireEmployeeCommandHandler>.GetDefault();
            HireEmployeeCommandData result = h.Handle(data);
            FormHelpers.RefreshScreens();
            if (result.errorMessage != "")
                MessageBox.Show(result.errorMessage);
        }

        private void calculateDept()
        {
            Department d = (Department)cbbDepartments.SelectedItem;
            this.cost = d.getDepartmentType().getEmployeeCost();
            this.duration = d.getHiringEmployeesTime();
            lblCostUn.Text = AOBHelpers.FormatToBrions(this.cost);
            lblTimeUn.Text = String.Format("{0}", this.duration);
        }

        private void calculateMax()
        {
            int maxDiv = (int)Math.Truncate((c.CurrentMoney / this.cost));
            Department d = (Department)cbbDepartments.SelectedItem;
            int employeesLeft = d.getMaxEmployeesLeft();
            this.max = (employeesLeft < maxDiv ? employeesLeft : maxDiv);
        }

        private void cbbDepartments_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.calculateDept();
            this.refreshMaxLabel();
            this.refreshEstimated();
        }

        private void edtQtd_ValueChanged(object sender, EventArgs e)
        {
            //this.refreshEstimated();
        }

        private void lblMax_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.refreshMaxLabel();
            this.edtQtd.Value = this.max;
        }

        private void LoadDepartments()
        {
            IEnumerable<Department> depts = BaseRepository<Department>.getAll().Where(d => d.Company == c.ID);
            cbbDepartments.DataSource = new BindingSource(depts, null);
            cbbDepartments.ValueMember = "ID";
            cbbDepartments.DisplayMember = "";
        }

        private void refreshEstimated()
        {
            TimeSpan estimated = TimeSpan.FromSeconds(Math.Round(Convert.ToDouble(this.edtQtd.Value) * this.duration.TotalSeconds));
            lblEstimated.Text = String.Format("Tempo Total Estimado: ({0})", estimated);
        }

        private void refreshMaxLabel()
        {
            this.calculateMax();
            //this.edtQtd.Maximum = this.max;
            lblMax.Text = String.Format("Max ({0})", this.max);
        }

        public bool hasActiveChild()
        {
            return false;
        }

        public void RefreshScreenInfo()
        {
            this.calculateDept();
        }

    }
}
