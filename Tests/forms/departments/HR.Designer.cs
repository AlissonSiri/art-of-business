﻿namespace Tests.forms.departments
{
    partial class HR
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HR));
            this.edtQtd = new System.Windows.Forms.NumericUpDown();
            this.lblMax = new System.Windows.Forms.LinkLabel();
            this.cbbDepartments = new System.Windows.Forms.ComboBox();
            this.btnHire = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.lblEstimated = new System.Windows.Forms.Label();
            this.grpb = new System.Windows.Forms.GroupBox();
            this.lblCostUn = new System.Windows.Forms.Label();
            this.lblTimeUn = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.edtQtd)).BeginInit();
            this.panel1.SuspendLayout();
            this.grpb.SuspendLayout();
            this.SuspendLayout();
            // 
            // edtQtd
            // 
            this.edtQtd.Location = new System.Drawing.Point(61, 85);
            this.edtQtd.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.edtQtd.Name = "edtQtd";
            this.edtQtd.Size = new System.Drawing.Size(61, 20);
            this.edtQtd.TabIndex = 0;
            this.edtQtd.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.edtQtd.ValueChanged += new System.EventHandler(this.edtQtd_ValueChanged);
            // 
            // lblMax
            // 
            this.lblMax.AutoSize = true;
            this.lblMax.Location = new System.Drawing.Point(138, 87);
            this.lblMax.Name = "lblMax";
            this.lblMax.Size = new System.Drawing.Size(54, 13);
            this.lblMax.TabIndex = 2;
            this.lblMax.TabStop = true;
            this.lblMax.Text = "Max (000)";
            this.lblMax.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblMax_LinkClicked);
            // 
            // cbbDepartments
            // 
            this.cbbDepartments.FormattingEnabled = true;
            this.cbbDepartments.Location = new System.Drawing.Point(61, 49);
            this.cbbDepartments.Name = "cbbDepartments";
            this.cbbDepartments.Size = new System.Drawing.Size(162, 21);
            this.cbbDepartments.TabIndex = 3;
            this.cbbDepartments.Text = "Escolha um departamento";
            this.cbbDepartments.SelectedIndexChanged += new System.EventHandler(this.cbbDepartments_SelectedIndexChanged);
            // 
            // btnHire
            // 
            this.btnHire.Location = new System.Drawing.Point(61, 125);
            this.btnHire.Name = "btnHire";
            this.btnHire.Size = new System.Drawing.Size(61, 23);
            this.btnHire.TabIndex = 4;
            this.btnHire.Text = "Contratar";
            this.btnHire.UseVisualStyleBackColor = true;
            this.btnHire.Click += new System.EventHandler(this.btnHire_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label4);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(562, 43);
            this.panel1.TabIndex = 6;
            // 
            // label4
            // 
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(0, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(562, 43);
            this.label4.TabIndex = 0;
            this.label4.Text = "Contratação de Funcionários";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblEstimated
            // 
            this.lblEstimated.AutoSize = true;
            this.lblEstimated.Location = new System.Drawing.Point(58, 180);
            this.lblEstimated.Name = "lblEstimated";
            this.lblEstimated.Size = new System.Drawing.Size(161, 13);
            this.lblEstimated.TabIndex = 7;
            this.lblEstimated.Text = "Tempo Total Estimado: 00:00:00";
            this.toolTip1.SetToolTip(this.lblEstimated, resources.GetString("lblEstimated.ToolTip"));
            // 
            // grpb
            // 
            this.grpb.Controls.Add(this.lblCostUn);
            this.grpb.Controls.Add(this.lblTimeUn);
            this.grpb.Controls.Add(this.label2);
            this.grpb.Controls.Add(this.label1);
            this.grpb.Location = new System.Drawing.Point(258, 42);
            this.grpb.Name = "grpb";
            this.grpb.Size = new System.Drawing.Size(154, 106);
            this.grpb.TabIndex = 8;
            this.grpb.TabStop = false;
            this.grpb.Text = "Por funcionário";
            // 
            // lblCostUn
            // 
            this.lblCostUn.AutoSize = true;
            this.lblCostUn.Location = new System.Drawing.Point(64, 62);
            this.lblCostUn.Name = "lblCostUn";
            this.lblCostUn.Size = new System.Drawing.Size(65, 13);
            this.lblCostUn.TabIndex = 3;
            this.lblCostUn.Text = "B$ 0.000,00";
            // 
            // lblTimeUn
            // 
            this.lblTimeUn.AutoSize = true;
            this.lblTimeUn.Location = new System.Drawing.Point(64, 36);
            this.lblTimeUn.Name = "lblTimeUn";
            this.lblTimeUn.Size = new System.Drawing.Size(34, 13);
            this.lblTimeUn.TabIndex = 2;
            this.lblTimeUn.Text = "00:00";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(37, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Custo:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Duração:";
            // 
            // HR
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.grpb);
            this.Controls.Add(this.lblEstimated);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btnHire);
            this.Controls.Add(this.cbbDepartments);
            this.Controls.Add(this.lblMax);
            this.Controls.Add(this.edtQtd);
            this.MinimumSize = new System.Drawing.Size(294, 218);
            this.Name = "HR";
            this.Size = new System.Drawing.Size(562, 218);
            ((System.ComponentModel.ISupportInitialize)(this.edtQtd)).EndInit();
            this.panel1.ResumeLayout(false);
            this.grpb.ResumeLayout(false);
            this.grpb.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.NumericUpDown edtQtd;
        private System.Windows.Forms.LinkLabel lblMax;
        private System.Windows.Forms.ComboBox cbbDepartments;
        private System.Windows.Forms.Button btnHire;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblEstimated;
        private System.Windows.Forms.GroupBox grpb;
        private System.Windows.Forms.Label lblCostUn;
        private System.Windows.Forms.Label lblTimeUn;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}
