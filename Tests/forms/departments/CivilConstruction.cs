﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tests.forms.core;

namespace Tests.forms.departments
{
    public partial class CivilConstruction : UserControl
    {

        public CivilConstruction()
        {
            InitializeComponent();
        }

        private void trackBar1_ValueChanged(object sender, EventArgs e)
        {
            lblValue.Text = String.Format("{0}%", tbarWorkRate.Value);
        }
    }
}
