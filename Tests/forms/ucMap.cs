﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Alisson.Core.Models;
using Alisson.Core.Repository;
using Tests.helpers;
using Tests.forms.core;

namespace Tests.forms
{
    public partial class ucMap : UserControl
    {
        private MapPoint middle;

        public ucMap(MapPoint middle)
        {
            this.middle = middle;
            InitializeComponent();
        }

        private void CalculeCellSizes()
        {
            int cellSize = Convert.ToInt32(Math.Round((double)grdMap.Width / grdMap.ColumnCount));
            for (int row = 0; row < grdMap.Rows.Count; row++)
                grdMap.Rows[row].Height = cellSize;
            for (int col = 0; col < grdMap.ColumnCount; col++)
                grdMap.Columns[col].Width = cellSize;
        }

        private void ucMap_Load(object sender, EventArgs e)
        {
            this.BuildMap();
        }

        private void GenerateColumns(int quantity)
        {
            for (int size = 0; size < quantity; size++)
            {
                DataGridViewImageColumn column = new DataGridViewImageColumn();
                column.ImageLayout = DataGridViewImageCellLayout.Stretch;
                column.ReadOnly = true;
                column.Resizable = DataGridViewTriState.False;
                column.Name = String.Format("Column{0}", size);
                grdMap.Columns.Add(column);
            }
        }

        private void GenerateRows(IEnumerable<IEnumerable<MapPoint>> matrix)
        {
            int row;
            int cell;
            foreach (IEnumerable<MapPoint> vector in matrix)
            {
                row = grdMap.Rows.Add();
                cell = 0; // Resets the cell index
                foreach (MapPoint point in vector)
                {
                    ((DataGridViewImageCell)grdMap.Rows[row].Cells[cell]).Value = imageList1.Images[point.getMapObject().GetImageName()];
                    ((DataGridViewImageCell)grdMap.Rows[row].Cells[cell]).ToolTipText = point.getToolTip();
                    ((DataGridViewImageCell)grdMap.Rows[row].Cells[cell]).Style.BackColor = Color.Red;
                    ((DataGridViewImageCell)grdMap.Rows[row].Cells[cell]).Tag = point.ID;
                    cell++;
                }
            }
        }

        private void grdMap_Resize(object sender, EventArgs e)
        {
            int size = Convert.ToInt32(Math.Round((double)grdMap.Width / 10));
            this.CalculeCellSizes();
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            this.BuildMap();
        }

        private void BuildMap()
        {
            int size = (int)edtMapSize.Value;
            IEnumerable<IEnumerable<MapPoint>> map = this.middle.GetNeighborsMatrix(size - 1);
            grdMap.Rows.Clear();
            grdMap.Columns.Clear();
            this.GenerateColumns(map.First().Count()); // Since all vectors will have same size, we can get any item (like the first).
            this.GenerateRows(map);
            this.CalculeCellSizes();
        }

        private void grdMap_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewImageCell cell = (DataGridViewImageCell) grdMap.SelectedCells[0];
            MapPoint point = BaseRepository<MapPoint>.getByID(Convert.ToInt32(cell.Tag));
            FormHelpers.PutUserControl(pnlMapObject, new ucMapPoint(point));
        }
    }
}
