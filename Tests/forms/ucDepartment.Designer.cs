﻿namespace Tests.forms
{
    partial class ucDepartment
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ucDepartment));
            this.lblTitle = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblEmployees = new System.Windows.Forms.Label();
            this.lblDescription = new System.Windows.Forms.Label();
            this.pnlDeptActions = new System.Windows.Forms.Panel();
            this.btnUpgrade = new System.Windows.Forms.Button();
            this.lblPriceNextLevel = new System.Windows.Forms.Label();
            this.lblDurationNextLevel = new System.Windows.Forms.Label();
            this.pnlLeft = new System.Windows.Forms.Panel();
            this.pnlRight = new System.Windows.Forms.Panel();
            this.pnlTitle = new System.Windows.Forms.Panel();
            this.pnlMain = new System.Windows.Forms.Panel();
            this.pnlScroll = new System.Windows.Forms.Panel();
            this.pnlDept = new System.Windows.Forms.Panel();
            this.lblLimit = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.pnlTitle.SuspendLayout();
            this.pnlMain.SuspendLayout();
            this.pnlScroll.SuspendLayout();
            this.pnlDept.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.Location = new System.Drawing.Point(0, 0);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(640, 46);
            this.lblTitle.TabIndex = 0;
            this.lblTitle.Text = "Department Type Name (Level 0)";
            this.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(43, 94);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Funcionários:";
            // 
            // lblEmployees
            // 
            this.lblEmployees.AutoSize = true;
            this.lblEmployees.Location = new System.Drawing.Point(147, 94);
            this.lblEmployees.Name = "lblEmployees";
            this.lblEmployees.Size = new System.Drawing.Size(30, 13);
            this.lblEmployees.TabIndex = 2;
            this.lblEmployees.Text = "Qtde";
            // 
            // lblDescription
            // 
            this.lblDescription.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblDescription.Location = new System.Drawing.Point(0, 0);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(640, 65);
            this.lblDescription.TabIndex = 3;
            this.lblDescription.Text = resources.GetString("lblDescription.Text");
            this.lblDescription.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pnlDeptActions
            // 
            this.pnlDeptActions.AutoScroll = true;
            this.pnlDeptActions.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlDeptActions.Location = new System.Drawing.Point(0, 215);
            this.pnlDeptActions.Name = "pnlDeptActions";
            this.pnlDeptActions.Size = new System.Drawing.Size(640, 170);
            this.pnlDeptActions.TabIndex = 4;
            // 
            // btnUpgrade
            // 
            this.btnUpgrade.Location = new System.Drawing.Point(46, 125);
            this.btnUpgrade.Name = "btnUpgrade";
            this.btnUpgrade.Size = new System.Drawing.Size(80, 23);
            this.btnUpgrade.TabIndex = 5;
            this.btnUpgrade.Text = "Evoluir";
            this.btnUpgrade.UseVisualStyleBackColor = true;
            this.btnUpgrade.Click += new System.EventHandler(this.btnUpgrade_Click);
            // 
            // lblPriceNextLevel
            // 
            this.lblPriceNextLevel.AutoSize = true;
            this.lblPriceNextLevel.Location = new System.Drawing.Point(147, 130);
            this.lblPriceNextLevel.Name = "lblPriceNextLevel";
            this.lblPriceNextLevel.Size = new System.Drawing.Size(97, 13);
            this.lblPriceNextLevel.TabIndex = 6;
            this.lblPriceNextLevel.Text = "B$ 000000 (Brions)";
            // 
            // lblDurationNextLevel
            // 
            this.lblDurationNextLevel.AutoSize = true;
            this.lblDurationNextLevel.Location = new System.Drawing.Point(261, 130);
            this.lblDurationNextLevel.Name = "lblDurationNextLevel";
            this.lblDurationNextLevel.Size = new System.Drawing.Size(94, 13);
            this.lblDurationNextLevel.TabIndex = 7;
            this.lblDurationNextLevel.Text = "00h00m00s (Time)";
            // 
            // pnlLeft
            // 
            this.pnlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlLeft.Location = new System.Drawing.Point(0, 0);
            this.pnlLeft.Name = "pnlLeft";
            this.pnlLeft.Size = new System.Drawing.Size(37, 385);
            this.pnlLeft.TabIndex = 8;
            // 
            // pnlRight
            // 
            this.pnlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnlRight.Location = new System.Drawing.Point(677, 0);
            this.pnlRight.Name = "pnlRight";
            this.pnlRight.Size = new System.Drawing.Size(44, 385);
            this.pnlRight.TabIndex = 9;
            // 
            // pnlTitle
            // 
            this.pnlTitle.Controls.Add(this.lblTitle);
            this.pnlTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTitle.Location = new System.Drawing.Point(0, 0);
            this.pnlTitle.Name = "pnlTitle";
            this.pnlTitle.Size = new System.Drawing.Size(640, 46);
            this.pnlTitle.TabIndex = 10;
            // 
            // pnlMain
            // 
            this.pnlMain.AutoScroll = true;
            this.pnlMain.Controls.Add(this.pnlScroll);
            this.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlMain.Location = new System.Drawing.Point(37, 0);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(640, 385);
            this.pnlMain.TabIndex = 11;
            // 
            // pnlScroll
            // 
            this.pnlScroll.AutoScroll = true;
            this.pnlScroll.Controls.Add(this.pnlDept);
            this.pnlScroll.Controls.Add(this.pnlTitle);
            this.pnlScroll.Controls.Add(this.pnlDeptActions);
            this.pnlScroll.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlScroll.Location = new System.Drawing.Point(0, 0);
            this.pnlScroll.Name = "pnlScroll";
            this.pnlScroll.Size = new System.Drawing.Size(640, 385);
            this.pnlScroll.TabIndex = 10;
            // 
            // pnlDept
            // 
            this.pnlDept.AutoScroll = true;
            this.pnlDept.Controls.Add(this.lblLimit);
            this.pnlDept.Controls.Add(this.label2);
            this.pnlDept.Controls.Add(this.lblDurationNextLevel);
            this.pnlDept.Controls.Add(this.lblPriceNextLevel);
            this.pnlDept.Controls.Add(this.btnUpgrade);
            this.pnlDept.Controls.Add(this.lblDescription);
            this.pnlDept.Controls.Add(this.lblEmployees);
            this.pnlDept.Controls.Add(this.label1);
            this.pnlDept.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlDept.Location = new System.Drawing.Point(0, 46);
            this.pnlDept.MinimumSize = new System.Drawing.Size(640, 169);
            this.pnlDept.Name = "pnlDept";
            this.pnlDept.Size = new System.Drawing.Size(640, 169);
            this.pnlDept.TabIndex = 11;
            // 
            // lblLimit
            // 
            this.lblLimit.AutoSize = true;
            this.lblLimit.Location = new System.Drawing.Point(261, 94);
            this.lblLimit.Name = "lblLimit";
            this.lblLimit.Size = new System.Drawing.Size(30, 13);
            this.lblLimit.TabIndex = 9;
            this.lblLimit.Text = "Qtde";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(206, 94);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Limite:";
            // 
            // ucDepartment
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.Controls.Add(this.pnlMain);
            this.Controls.Add(this.pnlRight);
            this.Controls.Add(this.pnlLeft);
            this.Name = "ucDepartment";
            this.Size = new System.Drawing.Size(721, 385);
            this.pnlTitle.ResumeLayout(false);
            this.pnlMain.ResumeLayout(false);
            this.pnlScroll.ResumeLayout(false);
            this.pnlDept.ResumeLayout(false);
            this.pnlDept.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblEmployees;
        private System.Windows.Forms.Label lblDescription;
        private System.Windows.Forms.Panel pnlDeptActions;
        private System.Windows.Forms.Button btnUpgrade;
        private System.Windows.Forms.Label lblPriceNextLevel;
        private System.Windows.Forms.Label lblDurationNextLevel;
        private System.Windows.Forms.Panel pnlLeft;
        private System.Windows.Forms.Panel pnlRight;
        private System.Windows.Forms.Panel pnlTitle;
        private System.Windows.Forms.Panel pnlMain;
        private System.Windows.Forms.Panel pnlDept;
        private System.Windows.Forms.Label lblLimit;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel pnlScroll;
    }
}
