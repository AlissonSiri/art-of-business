﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Alisson.Core.Models;
using Tests.forms.core;

namespace Tests.forms
{
    public partial class ucNPC : UserControl, IGameInterface
    {

        Npc npc;

        public ucNPC(Npc npc)
        {
            InitializeComponent();
            this.npc = npc;
            lblTitle.Text = npc.GetDisplayText();
            lblDescription.Text = npc.GetDescription();
            this.RefreshScreenInfo();
        }

        public void RefreshScreenInfo()
        {
            this.lblEstoque.Text = "Estoque: ";
        }

    }
}
