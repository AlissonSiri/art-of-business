﻿using System;
using System.Linq;
using System.Windows.Forms;
using Alisson.Core.Models;
using Alisson.Core.Repository;
using Tests.forms.departments;
using Tests.helpers;
using Alisson.Core.IoC;
using Alisson.Core.Services.Commands.UpgradeDepartment;
using Tests.forms.core;
using Alisson.Core.Services;
using Alisson.Core.Types;

namespace Tests.forms
{
    public partial class ucDepartment : UserControl, IGameInterface
    {
        public static Department department;
        public ucDepartment(Department d)
        {
            InitializeComponent();
            department = d;
            this.loadDetails();
            FormHelpers.PutUserControl(this.pnlDeptActions, DepartmentsFormFactory.make((DepartmentTypes)d.Type));
        }

        private void loadDetails()
        {
            DepartmentType t = BaseRepository<DepartmentType>.getAll().Where(dt => dt.ID == department.Type).First();
            lblTitle.Text = t.Name + String.Format(" (Nível {0})", department.CurrentLevel);
            lblDescription.Text = t.Description;
            lblEmployees.Text = department.Employees.ToString();
            lblLimit.Text = department.getMaxEmployees().ToString();
            if (department.isAtMaximumLevel())
            {
                lblPriceNextLevel.Text = "";
                lblDurationNextLevel.Text = "";
            }
            else
            {
                lblPriceNextLevel.Text = AOBHelpers.FormatToBrions(department.getNextLevelCost());
                lblDurationNextLevel.Text = department.getNextLevelTime().ToString();
            }

        }

        private void btnUpgrade_Click(object sender, EventArgs e)
        {
            UpgradeDepartmentCommandData data = new UpgradeDepartmentCommandData(department);
            UpgradeDepartmentCommandHandler h = GenericFactory<UpgradeDepartmentCommandHandler>.GetDefault();
            UpgradeDepartmentCommandData result = h.Handle(data);
            FormHelpers.RefreshScreens();
            if (result.errorMessage != "")
                MessageBox.Show(result.errorMessage);
        }

        public void RefreshScreenInfo()
        {
            this.loadDetails();
        }

    }
}
