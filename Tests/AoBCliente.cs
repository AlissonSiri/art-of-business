﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Text;
using System.Threading.Tasks;

namespace Tests
{
    public static class AoBCliente
    {

        private static string requestUri = "http://localhost:1346";

        public static JToken GetRequest(string uri) 
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(requestUri);
                HttpResponseMessage http = client.GetAsync("/api/" + uri).Result;

                if (http.IsSuccessStatusCode)
                {
                    string json = http.Content.ReadAsStringAsync().Result;
                    return JContainer.Parse(json);
                }
                else
                {
                    string content = http.Content.ReadAsStringAsync().Result;
                    throw new Exception(String.Format("oops, an error occurred, here's the raw response: {0}", content));
                }

            }
        }

        public static object PostRequest(string uri, JObject json)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(requestUri);
                JsonMediaTypeFormatter f = new JsonMediaTypeFormatter();
                HttpResponseMessage http = client.PostAsync("/api/" + uri, JsonConvert.SerializeObject(json, Formatting.Indented), f).Result;

                if (http.IsSuccessStatusCode)
                {
                    return (object) http.Content.ReadAsStringAsync().Result;
                }
                else
                {
                    string content = http.Content.ReadAsStringAsync().Result;
                    return String.Format("oops, an error occurred, here's the raw response: {0}", content);
                }
            }
        }
    }
}
