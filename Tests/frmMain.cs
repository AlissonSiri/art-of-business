﻿using Alisson.Core.Models;
using System;
using System.Linq;
using System.Windows.Forms;
using Tests.forms;
using Alisson.Core.Repository;
using Alisson.Core.Services.Commands.RegisterUser;
using Alisson.Core.IoC;
using Tests.helpers;
using Tests.forms.core;
using Alisson.Core.Services.Commands.ExecuteEvents;
using Alisson.Core.Services;
using Alisson.Core.Services.Commands.IncreaseMap;
using System.Collections.Generic;
using Alisson.Core.Services.Commands.RandomUsers;
using Alisson.Core.Database;
using Alisson.Core;

namespace Tests
{
    public partial class frmMain : Form, IGameInterface
    {

        private readonly ICommandHandler<RegisterUserCommandData> reg;
        private readonly ICommandHandler<ExecuteEventsCommandData> exec;
        private readonly ICommandHandler<IncreaseMapCommandData> im;
        private readonly ICommandHandler<RandomUsersCommandData> random;
        public IGameInterface activeChild { get; set; }
        public IGameInterface owner { get; set; }

        public frmMain(ICommandHandler<RegisterUserCommandData> reg, ICommandHandler<ExecuteEventsCommandData> exec, ICommandHandler<IncreaseMapCommandData> im, ICommandHandler<RandomUsersCommandData> random)
        {
            this.reg = reg;
            this.exec = exec;
            this.im = im;
            this.random = random;
            InitializeComponent();
            ConnectionConfigAOB.configure();
            this.Login();
            FormHelpers.mainClient = this.pnlClient;
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            string name = edtName.Text;
            string pass = edtSenha.Text;
            string company = edtEmpresa.Text;
            int type = (int)cbbTypes.SelectedValue;

            try
            {
                RegisterUserCommandData data = new RegisterUserCommandData(name, company, pass, type, Helpers.GetLocalIP());

                Session.UserID = this.reg.Handle(data).UserID;

                MessageBox.Show("Cadastro efetuado com sucesso!");
                //JObject json = new JObject();
                //json.Add(new JProperty("User", JsonConvert.SerializeObject(obj, Formatting.Indented)));
                //json.Add(new JProperty("CompanyType", type));
                //Session.UserId = Convert.ToInt32(AoBCliente.PostRequest("users", json));
                //return true;

            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("Ocorreu um erro inesperado: {0}", ex.Message));
            }

        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            this.Login();
        }

        private void Login()
        {
            int ID = User.Login(edtLogin.Text, edtPassLogin.Text).ID;

            if (ID > 0)
            {
                Session.UserID = ID;
                this.exec.Handle(new ExecuteEventsCommandData(Session.UserID));
                //MessageBox.Show("Login efetuado com sucesso!");
            }
            else
            {
                MessageBox.Show("Login e/ou senha digitado(s) incorretamente!!");
            }
        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

            //SimpleInjector.Container container = new SimpleInjector.Container();
            //BootStrapper.Configure(container);
            //container.Verify();
            //using (container.BeginLifetimeScope())
            //{
            //    IncreaseMapCommandHandler entryPoint = container.GetInstance<IncreaseMapCommandHandler>();
            //    RegisterUserCommand user = new RegisterUserCommand() { User = new User() { Name = "A", Password = "A", CompanyName = "A" } };
            //    entryPoint.Handle(user);
            //}

            //JToken json = AoBCliente.GetRequest("act/repository/industries");
            CompanyType[] comps = CompanyType.getIndustries().ToArray();
            cbbTypes.DataSource = new BindingSource(comps, null);
            cbbTypes.DisplayMember = "Name";
            cbbTypes.ValueMember = "ID";

            FormHelpers.PutUserControl(this.pnlClient, new ucCompany());

        }

        private void cbbTypes_Leave(object sender, EventArgs e)
        {
            if (cbbTypes.SelectedIndex == -1)
                cbbTypes.SelectedIndex = 0;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FormHelpers.PutUserControl(this.pnlClient, new ucCompany());
        }

        public void RefreshScreenInfo()
        {
        }

        public bool hasActiveChild()
        {
            return this.activeChild != null;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                im.Handle(new IncreaseMapCommandData() { Radius = Convert.ToInt32(edtRadius.Text.Trim()) });
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            User u = new User(Session.UserID);
            Company comp = u.getCompanies().First();
            MapPoint current = MapPoint.Where(comp);
            List<MapPoint> places = MapPoint.GetAvailablePlaces(comp).ToList();
            if (places.Count() == 0)
            {
                new IncreaseMapCommandHandler().Handle(new IncreaseMapCommandData() { Radius = 10 });
                places = MapPoint.GetAvailablePlaces(comp).ToList();
            }
            MapPoint m = Helpers.GetRandom<MapPoint>(places);
            m.setObject(comp);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            random.Handle(new RandomUsersCommandData(Convert.ToInt32(edtQtdUsers.Text)));
        }

        private void button5_Click(object sender, EventArgs e)
        {
            FormHelpers.PutUserControl(this.pnlClient, new ucMap(MapPoint.Where(BaseRepository<Company>.getByID(Session.SelectedCompanyID))));
        }
    }
}
