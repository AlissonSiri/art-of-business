[assembly: WebActivator.PostApplicationStartMethod(typeof(AOB.Server.App_Start.SimpleInjectorWebApiInitializer), "Initialize")]

namespace AOB.Server.App_Start
{
    using System.Web.Http;
    using SimpleInjector;
    using SimpleInjector.Integration.WebApi;
    using Alisson.Core.IoC;
    using System.Web.Mvc;
    using SimpleInjector.Integration.Web.Mvc;
    
    public static class SimpleInjectorWebApiInitializer
    {
        /// <summary>Initialize the container and register it as MVC3 Dependency Resolver.</summary>
        public static void Initialize()
        {
            // Did you know the container can diagnose your configuration? Go to: https://bit.ly/YE8OJj.
            var container = new Container();
            
            InitializeContainer(container);
       
            container.Verify();
            
            GlobalConfiguration.Configuration.DependencyResolver =
                new SimpleInjectorWebApiDependencyResolver(container);
        }
     
        private static void InitializeContainer(Container container)
        {

            container.RegisterWebApiControllers(GlobalConfiguration.Configuration);

            BootStrapper.Configure(container);

        }
    }
}