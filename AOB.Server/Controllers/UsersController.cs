﻿using Alisson.Core;
using Alisson.Core.IoC;
using Alisson.Core.Models;
using Alisson.Core.Repository;
using Alisson.Core.Services;
using Alisson.Core.Services.Commands.RegisterUser;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Linq;
using System.Web.Http;

namespace AOB.Server.Controllers
{
    public class UsersController : ApiController
    {

        private readonly ICommandHandler<RegisterUserCommandData> handler;

        // GET: api/Users2
        public User[] Get()
        {
            return BaseRepository<User>.getAll().ToArray();
        }
        public UsersController(ICommandHandler<RegisterUserCommandData> handler)
            {
                this.handler = handler;
            }
        // GET: api/Users/5
        public User Get(int id)
        {
            return new User(id);
        }

        //[HttpGet]
        //[ActionName("Lists")]
        //public BaseObject[] GetLists(string name, int id)
        //{
        //    //return UserRepository<>.getAll(id, name);
        //}

        // POST: api/Users
        public int Post([FromBody] string json)
        {
            JObject obj = JObject.Parse(json);
            User user = JsonConvert.DeserializeObject<User>(obj["User"].ToString());
            RegisterUserCommandData c = new RegisterUserCommandData(user.Name, user.CompanyName, user.Password, Convert.ToInt32(obj["CompanyType"]), Helpers.GetLocalIP());
            this.handler.Handle(c);
            return 1;
        }

        // PUT: api/Users/5
        public void Put(int id, [FromBody] string json)
        {
            //JObject o = JObject.Parse(json);
            User user = new User(id);
            user.setAttributesFromJson(json);

            user.save(false, true);
        }

        // DELETE: api/Users/5
        public string Delete(int id)
        {
            User user = new User(id);
            user.delete();
            return "User " + id + " deleted successfully!";
        }
    }
}
