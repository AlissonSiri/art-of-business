﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Threading.Tasks;
using System.Net.Http.Headers;
using Alisson.Core.Models;
using System.Web;
using System.IO;
using Newtonsoft.Json.Linq;
using System.Net.Http.Formatting;
using Newtonsoft.Json;

namespace AOB.Server.Controllers
{
    public class TestController : ApiController
    {
        // GET: api/Test
        public string Get()
        {
            User obj = new User() { Name = "Alisson", CompanyName = "TESTE", Password = "123" };
            JObject json = new JObject();
            json.Add(new JProperty("User", JsonConvert.SerializeObject(obj, Formatting.Indented)));
            json.Add(new JProperty("CompanyType", 16));

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:1560");
                JsonMediaTypeFormatter f = new JsonMediaTypeFormatter();
                var result = client.PostAsync(String.Format("/api/{0}", obj.getTableName()), JsonConvert.SerializeObject(json, Formatting.Indented), f).Result;
                if (result.IsSuccessStatusCode)
                {
                    int id = Convert.ToInt32(result.Content.ReadAsStringAsync().Result);
                    obj = new User(id);
                    obj.Name = "Alisson_New";
                    result = client.PutAsync(String.Format("/api/{0}/{1}", obj.getTableName(), obj.ID), JsonConvert.SerializeObject(obj, Formatting.Indented), f).Result;
                    if (result.IsSuccessStatusCode)
                    {
                        result = client.DeleteAsync(String.Format("/api/{0}/{1}", obj.getTableName(), obj.ID)).Result;
                        if (result.IsSuccessStatusCode)
                            return result.Content.ReadAsStringAsync().Result;
                        else
                        {
                            string content = result.Content.ReadAsStringAsync().Result;
                            return String.Format("oops, an error occurred, here's the raw response: {0}", content);
                        }
                    }
                    else
                    {
                        string content = result.Content.ReadAsStringAsync().Result;
                        return String.Format("oops, an error occurred, here's the raw response: {0}", content);
                    }
                }
                else
                {
                    string content = result.Content.ReadAsStringAsync().Result;
                    return String.Format("oops, an error occurred, here's the raw response: {0}", content);
                }
            }
        }

    }
}
