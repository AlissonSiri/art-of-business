﻿using Alisson.Core.Repository;
using Alisson.Core.Models;
using System.Linq;
using System.Web.Http;
using Alisson.Core.Types;

namespace AOB.Server.Controllers
{
    public class RepositoryController : ApiController
    {

        [HttpGet]
        [ActionName("Industries")]
        public CompanyType[] GetIndustries()
        {
            return BaseRepository<CompanyType>.getAll().Where(c => c.SegmentType == (int)SegmentTypes.Industry + 1).ToArray<CompanyType>();
            //JObject json = new JObject();
            //json.Add(new JProperty("Result", JsonConvert.SerializeObject(comps, Formatting.Indented)));
            //return JsonConvert.SerializeObject(json, Formatting.Indented);
        }

        [HttpGet]
        public User[] Users()
        {
            return BaseRepository<User>.getAll().ToArray();
        }
    }
}
